USE [ont_db_reports_home]
GO
/****** Object:  StoredProcedure [dbo].[update_Table_relT_OR]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[update_Table_relT_OR]
GO
/****** Object:  StoredProcedure [dbo].[update_Table_relT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[update_Table_relT]
GO
/****** Object:  StoredProcedure [dbo].[update_Table_orgT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[update_Table_orgT]
GO
/****** Object:  StoredProcedure [dbo].[update_Table_attT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[update_Table_attT]
GO
/****** Object:  StoredProcedure [dbo].[sysproc_reorganize_indexes]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[sysproc_reorganize_indexes]
GO
/****** Object:  StoredProcedure [dbo].[sysproc_Code]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[sysproc_Code]
GO
/****** Object:  StoredProcedure [dbo].[orgproc_Fill_DateTable]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[orgproc_Fill_DateTable]
GO
/****** Object:  StoredProcedure [dbo].[insert_Object]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[insert_Object]
GO
/****** Object:  StoredProcedure [dbo].[initialize_Tables]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[initialize_Tables]
GO
/****** Object:  StoredProcedure [dbo].[initialize_Table_relT_OR]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[initialize_Table_relT_OR]
GO
/****** Object:  StoredProcedure [dbo].[initialize_Table_relT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[initialize_Table_relT]
GO
/****** Object:  StoredProcedure [dbo].[initialize_Table_orgT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[initialize_Table_orgT]
GO
/****** Object:  StoredProcedure [dbo].[initialize_Table_AttT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[initialize_Table_AttT]
GO
/****** Object:  StoredProcedure [dbo].[initialize_Table]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[initialize_Table]
GO
/****** Object:  StoredProcedure [dbo].[import_BulkFile_RelationType_OR]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[import_BulkFile_RelationType_OR]
GO
/****** Object:  StoredProcedure [dbo].[import_BulkFile_RelationType]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[import_BulkFile_RelationType]
GO
/****** Object:  StoredProcedure [dbo].[import_BulkFile_Class]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[import_BulkFile_Class]
GO
/****** Object:  StoredProcedure [dbo].[import_BulkFile_Attribute]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[import_BulkFile_Attribute]
GO
/****** Object:  StoredProcedure [dbo].[import_BulkFile]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[import_BulkFile]
GO
/****** Object:  StoredProcedure [dbo].[finalize_Tables]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[finalize_Tables]
GO
/****** Object:  StoredProcedure [dbo].[finalize_Table_relT_OR]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[finalize_Table_relT_OR]
GO
/****** Object:  StoredProcedure [dbo].[finalize_Table_relT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[finalize_Table_relT]
GO
/****** Object:  StoredProcedure [dbo].[finalize_Table_orgT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[finalize_Table_orgT]
GO
/****** Object:  StoredProcedure [dbo].[finalize_Table_attT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[finalize_Table_attT]
GO
/****** Object:  StoredProcedure [dbo].[finalize_Table]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[finalize_Table]
GO
/****** Object:  StoredProcedure [dbo].[delete_ImportTables]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[delete_ImportTables]
GO
/****** Object:  StoredProcedure [dbo].[create_Table_relT_OR]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[create_Table_relT_OR]
GO
/****** Object:  StoredProcedure [dbo].[create_Table_relT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[create_Table_relT]
GO
/****** Object:  StoredProcedure [dbo].[create_Table_orgT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[create_Table_orgT]
GO
/****** Object:  StoredProcedure [dbo].[create_Table_attT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[create_Table_attT]
GO
/****** Object:  StoredProcedure [dbo].[create_BulkTable_relT_OR]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[create_BulkTable_relT_OR]
GO
/****** Object:  StoredProcedure [dbo].[create_BulkTable_relT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[create_BulkTable_relT]
GO
/****** Object:  StoredProcedure [dbo].[create_BulkTable_orgT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[create_BulkTable_orgT]
GO
/****** Object:  StoredProcedure [dbo].[create_BulkTable_attT]    Script Date: 17.03.2024 09:18:02 ******/
DROP PROCEDURE IF EXISTS [dbo].[create_BulkTable_attT]
GO
/****** Object:  Table [dbo].[systbl_Tables]    Script Date: 17.03.2024 09:18:02 ******/
DROP TABLE IF EXISTS [dbo].[systbl_Tables]
GO
/****** Object:  UserDefinedFunction [dbo].[IsSubstraction]    Script Date: 17.03.2024 09:18:02 ******/
DROP FUNCTION IF EXISTS [dbo].[IsSubstraction]
GO
/****** Object:  UserDefinedFunction [dbo].[IsMultiplication]    Script Date: 17.03.2024 09:18:02 ******/
DROP FUNCTION IF EXISTS [dbo].[IsMultiplication]
GO
/****** Object:  UserDefinedFunction [dbo].[IsDivision]    Script Date: 17.03.2024 09:18:02 ******/
DROP FUNCTION IF EXISTS [dbo].[IsDivision]
GO
/****** Object:  UserDefinedFunction [dbo].[IsAddition]    Script Date: 17.03.2024 09:18:02 ******/
DROP FUNCTION IF EXISTS [dbo].[IsAddition]
GO
/****** Object:  UserDefinedFunction [dbo].[GetGermanWeekdayName]    Script Date: 17.03.2024 09:18:02 ******/
DROP FUNCTION IF EXISTS [dbo].[GetGermanWeekdayName]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_HTMLDecode]    Script Date: 17.03.2024 09:18:02 ******/
DROP FUNCTION IF EXISTS [dbo].[fn_HTMLDecode]
GO
/****** Object:  Schema [import]    Script Date: 17.03.2024 09:18:02 ******/
DROP SCHEMA IF EXISTS [import]
GO
/****** Object:  Schema [import]    Script Date: 17.03.2024 09:18:02 ******/
CREATE SCHEMA [import]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_HTMLDecode]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_HTMLDecode](
    @vcWhat NVARCHAR(MAX)
    ,@toDecodeMainISOSymbols bit = 1
    ,@toDecodeISOChars bit = 1
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
    DECLARE @vcResult NVARCHAR(MAX);
    DECLARE @siPos INT ,@vcEncoded NVARCHAR(9) ,@siChar INT;
    SET @vcResult = RTRIM(LTRIM(CAST(REPLACE(@vcWhat COLLATE Latin1_General_BIN, CHAR(0), '') AS NVARCHAR(MAX))));
    SELECT @vcResult = REPLACE(REPLACE(@vcResult, '&#160;', ' '), '&nbsp;', ' ');
    IF @vcResult = '' RETURN @vcResult;

    declare @s varchar(35);
    declare @n int; set @n = 6;
    declare @i int;

    while @n > 1
    begin
        set @s = '';
        set @i=1;
        while @i<=@n
        begin
            set @s = @s + '[0-9]';
            set @i = @i + 1;
        end
        set @s = '%&#' + @s + '%';
        SELECT @siPos = PATINDEX(@s, @vcResult);
        WHILE @siPos > 0
        BEGIN
            SELECT @vcEncoded = SUBSTRING(@vcResult, @siPos, @n+3)
                ,@siChar = CAST(SUBSTRING(@vcEncoded, 3, @n) AS INT)
                ,@vcResult = REPLACE(@vcResult, @vcEncoded, NCHAR(@siChar))
                ,@siPos = PATINDEX(@s, @vcResult);
        END
        set @n = @n - 1;
    end

    if @toDecodeMainISOSymbols=1
    begin
        select @vcResult = REPLACE(REPLACE(@vcResult, NCHAR(160), ' '), CHAR(160), ' ');
        select @vcResult = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@vcResult, '&amp;', '&'), '&quot;', '"'), '&lt;', '<'), '&gt;', '>'), '&amp;amp;', '&'),'&rdquo;','”'),'&bdquo;','„'),'&ndash;','–'),'&mdash;','—');
        select @vcResult = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@vcResult,'&lsquo;','‘'),'&rsquo;','’'),'&bull;','•'),'&hellip;','…'),'&permil;','‰') COLLATE Latin1_General_BIN,'&prime;','′') COLLATE Latin1_General_BIN,'&Prime;','″'),'&circ;','ˆ'),'&tilde;','˜'),'&nbsp;',' ');
    end

    if @toDecodeISOChars=1
    begin
        select @vcResult = REPLACE(REPLACE(REPLACE(REPLACE(@vcResult COLLATE Latin1_General_BIN,'&Scaron;','Š') COLLATE Latin1_General_BIN,'&scaron;','š') COLLATE Latin1_General_BIN,'&Ccedil;','Ç') COLLATE Latin1_General_BIN,'&ccedil;','ç');
        select @vcResult = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@vcResult,'&Agrave;','À') COLLATE Latin1_General_BIN,'&agrave;','à') COLLATE Latin1_General_BIN,'&Aacute;','Á') COLLATE Latin1_General_BIN,'&aacute;','á') COLLATE Latin1_General_BIN,'&Acirc;','Â') COLLATE Latin1_General_BIN,'&acirc;','â') COLLATE Latin1_General_BIN,'&Atilde;','Ã') COLLATE Latin1_General_BIN,'&atilde;','ã') COLLATE Latin1_General_BIN,'&Auml;','Ä') COLLATE Latin1_General_BIN,'&auml;','ä') COLLATE Latin1_General_BIN,'&Aring;','Å') COLLATE Latin1_General_BIN,'&aring;','å') COLLATE Latin1_General_BIN,'&AElig;','Æ') COLLATE Latin1_General_BIN,'&aelig;','æ');
        select @vcResult = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@vcResult COLLATE Latin1_General_BIN,'&Egrave','È') COLLATE Latin1_General_BIN,'&egrave','è') COLLATE Latin1_General_BIN,'&Eacute;','É') COLLATE Latin1_General_BIN,'&eacute;','é') COLLATE Latin1_General_BIN,'&Ecirc;','Ê') COLLATE Latin1_General_BIN,'&ecirc;','ê') COLLATE Latin1_General_BIN,'&Euml;','Ë') COLLATE Latin1_General_BIN,'&euml;','ë');
        select @vcResult = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@vcResult COLLATE Latin1_General_BIN,'&Igrave;','Ì') COLLATE Latin1_General_BIN,'&igrave;','ì') COLLATE Latin1_General_BIN,'&Iacute;','Í') COLLATE Latin1_General_BIN,'&iacute;','í') COLLATE Latin1_General_BIN,'&Icirc;','Î') COLLATE Latin1_General_BIN,'&icirc;','î') COLLATE Latin1_General_BIN,'&Iuml;','Ï') COLLATE Latin1_General_BIN,'&iuml;','ï');
        select @vcResult = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@vcResult COLLATE Latin1_General_BIN,'&Ograve;','Ò') COLLATE Latin1_General_BIN,'&ograve;','ò') COLLATE Latin1_General_BIN,'&Oacute;','Ó') COLLATE Latin1_General_BIN,'&oacute;','ó') COLLATE Latin1_General_BIN,'&Ocirc;','Ô') COLLATE Latin1_General_BIN,'&ocirc;','ô') COLLATE Latin1_General_BIN,'&Otilde;','Õ') COLLATE Latin1_General_BIN,'&otilde;','õ') COLLATE Latin1_General_BIN,'&Ouml;','Ö') COLLATE Latin1_General_BIN,'&ouml;','ö') COLLATE Latin1_General_BIN,'&Oslash','Ø') COLLATE Latin1_General_BIN,'&oslash','ø');
        select @vcResult = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@vcResult COLLATE Latin1_General_BIN,'&Ugrave;','Ù') COLLATE Latin1_General_BIN,'&ugrave;','ù') COLLATE Latin1_General_BIN,'&Uacute;','Ú') COLLATE Latin1_General_BIN,'&uacute;','ú') COLLATE Latin1_General_BIN,'&Ucirc;','Û') COLLATE Latin1_General_BIN,'&ucirc;','û') COLLATE Latin1_General_BIN,'&Uuml;','Ü') COLLATE Latin1_General_BIN,'&uuml;','ü');
        select @vcResult = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@vcResult COLLATE Latin1_General_BIN,'&ETH;','Ð') COLLATE Latin1_General_BIN,'&eth;','ð') COLLATE Latin1_General_BIN,'&Ntilde;','Ñ') COLLATE Latin1_General_BIN,'&ntilde;','ñ') COLLATE Latin1_General_BIN,'&Yacute;','Ý') COLLATE Latin1_General_BIN,'&yacute;','ý') COLLATE Latin1_General_BIN,'&THORN;','Þ') COLLATE Latin1_General_BIN,'&thorn;','þ') COLLATE Latin1_General_BIN,'&szlig;','ß');
    end
    RETURN @vcResult;
END
-- test:
-- select dbo.fn_HTMLDecode(N'A fine example of man and nature co-existing is Slovenia&#8217;s ecological tourist farms.',1,1)
-- select dbo.fn_HTMLDecode(N'm0 &#50752;&#51064;&#48516;&#50556;&#50640;&#49436; m1 &#44032;&#51109; m2 &#50689;&#54693;&#47141; m3&#51080;&#45716;m10',1,1)
GO
/****** Object:  UserDefinedFunction [dbo].[GetGermanWeekdayName]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetGermanWeekdayName]
(
	-- Add the parameters for the function here
	@DateTimeStamp		DATETIME
)
RETURNS NVARCHAR(255)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @DayName		NVARCHAR(255)
	SET @DayName = CASE WHEN DATENAME(weekday, @DateTimeStamp) = 'Monday'
					THEN 'Montag'
					ELSE 
						CASE WHEN DATENAME(weekday, @DateTimeStamp) = 'Tuesday'
						THEN 'Dienstag'
						ELSE 
							CASE WHEN DATENAME(weekday, @DateTimeStamp) = 'Wednesday'
							THEN 'Mittwoch'
							ELSE 
								CASE WHEN DATENAME(weekday, @DateTimeStamp) = 'Thursday'
								THEN 'Donnerstag'
								ELSE 
									CASE WHEN DATENAME(weekday, @DateTimeStamp) = 'Friday'
									THEN 'Freitag'
									ELSE 
										CASE WHEN DATENAME(weekday, @DateTimeStamp) = 'Saturday'
										THEN 'Samstag'
										ELSE 
											CASE WHEN DATENAME(weekday, @DateTimeStamp) = 'Sunday'
											THEN 'Sonntag'
											ELSE
												NULL
											END
										END
									END
								END
							END
						END
					END

	-- Return the result of the function
	RETURN @DayName

END
GO
/****** Object:  UserDefinedFunction [dbo].[IsAddition]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[IsAddition]
(
	-- Add the parameters for the function here
	@GUID_Function VARCHAR(36)
)
RETURNS Bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @IsFunction	BIT
	DECLARE @Count		INT

	-- Add the T-SQL statements to compute the return value here

	SELECT @Count = COUNT(*)
	FROM orgview_MathFunc_Add
	WHERE GUID_Funktion = @GUID_Function

	IF @Count >0 
	BEGIN
		SET @IsFunction = 1
	END
	ELSE
	BEGIN
		SET @IsFunction = 0
	END

	-- Return the result of the function
	RETURN @IsFunction

END
GO
/****** Object:  UserDefinedFunction [dbo].[IsDivision]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[IsDivision]
(
	-- Add the parameters for the function here
	@GUID_Function VARCHAR(36)
)
RETURNS Bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @IsFunction	BIT
	DECLARE @Count		INT

	-- Add the T-SQL statements to compute the return value here

	SELECT @Count = COUNT(*)
	FROM orgview_MathFunc_Div
	WHERE GUID_Funktion = @GUID_Function

	IF @Count >0 
	BEGIN
		SET @IsFunction = 1
	END
	ELSE
	BEGIN
		SET @IsFunction = 0
	END

	-- Return the result of the function
	RETURN @IsFunction

END

GO
/****** Object:  UserDefinedFunction [dbo].[IsMultiplication]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[IsMultiplication]
(
	-- Add the parameters for the function here
	@GUID_Function VARCHAR(36)
)
RETURNS Bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @IsFunction	BIT
	DECLARE @Count		INT

	-- Add the T-SQL statements to compute the return value here

	SELECT @Count = COUNT(*)
	FROM orgview_MathFunc_Mult
	WHERE GUID_Funktion = @GUID_Function

	IF @Count >0 
	BEGIN
		SET @IsFunction = 1
	END
	ELSE
	BEGIN
		SET @IsFunction = 0
	END

	-- Return the result of the function
	RETURN @IsFunction

END

GO
/****** Object:  UserDefinedFunction [dbo].[IsSubstraction]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[IsSubstraction]
(
	-- Add the parameters for the function here
	@GUID_Function VARCHAR(36)
)
RETURNS Bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @IsFunction	BIT
	DECLARE @Count		INT

	-- Add the T-SQL statements to compute the return value here

	SELECT @Count = COUNT(*)
	FROM orgview_MathFunc_Sub
	WHERE GUID_Funktion = @GUID_Function

	IF @Count >0 
	BEGIN
		SET @IsFunction = 1
	END
	ELSE
	BEGIN
		SET @IsFunction = 0
	END

	-- Return the result of the function
	RETURN @IsFunction

END

GO
/****** Object:  Table [dbo].[systbl_Tables]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[systbl_Tables](
	[Type] [nvarchar](128) NOT NULL,
	[Name_Table] [nvarchar](128) NOT NULL,
	[Exist] [bit] NOT NULL,
	[Sync_Start] [datetime] NULL,
	[Sync_End] [datetime] NULL,
 CONSTRAINT [PK_systbl_Tables] PRIMARY KEY CLUSTERED 
(
	[Type] ASC,
	[Name_Table] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[create_BulkTable_attT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[create_BulkTable_attT]
	-- Add the parameters for the stored procedure here
	 @Name_Class			nvarchar(128)
	,@Name_AttributeType	nvarchar(128)
	,@DataType				nvarchar(128)
	,@Length				nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @SQL			nvarchar(max) = ''
	DECLARE @SQL_Table		nvarchar(max) = ''
	DECLARE @Name_Table		nvarchar(128)
	DECLARE @SET_UPdate		nvarchar(max) = ''
	DECLARE @SELECT			nvarchar(max) = ''
	DECLARE @WHERE_Insert	nvarchar(max) = ''
	DECLARE @JOIN			nvarchar(max) = ''
	
	SET @Name_Class = REPLACE(@Name_Class,'','')
	SET @Name_Class = REPLACE(@Name_Class,' ','')
	SET @Name_Class = REPLACE(@Name_Class,':','')
	SET @Name_Class = REPLACE(@Name_Class,'-','')
	SET @Name_Class = REPLACE(@Name_Class,'(','')
	SET @Name_Class = REPLACE(@Name_Class,')','')
	SET @Name_Class = REPLACE(@Name_Class,'/','')
	SET @Name_Class = REPLACE(@Name_Class,'\','')
	SET @Name_Class = REPLACE(@Name_Class,'>','')
	SET @Name_Class = REPLACE(@Name_Class,'*','')
	SET @Name_Class = REPLACE(@Name_Class,'.','')
	SET @Name_Class = REPLACE(@Name_Class,'_','')
	SET @Name_Class = REPLACE(@Name_Class,',','')
	SET @Name_Class = REPLACE(@Name_Class,'"','')
	SET @Name_Class = REPLACE(@Name_Class,'&','')
	SET @Name_Class = REPLACE(@Name_Class,'+','')
	SET @Name_Class = REPLACE(@Name_Class,'%','')
	
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,' ','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,':','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'-','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'(','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,')','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'/','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'\','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'>','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'*','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'.','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,',','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'"','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'&','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'+','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'%','_')
	

	SET @SQL = @SQL + '
	IF OBJECT_ID(''attT_' + @Name_Class + '_' + @Name_AttributeType + ''', N''U'') IS NULL 
	BEGIN
	
		CREATE TABLE [attT_' + @Name_Class + '_' + @Name_AttributeType + ']
		(
			[GUID_Attribute_' + @Name_AttributeType + '] varchar(36) NOT NULL,
			[GUID_AT_' + @Name_AttributeType + '] varchar(36) NOT NULL,
			[Name_AT_' + @Name_AttributeType + '] nvarchar(255) NOT NULL,
			[GUID_Object] varchar(36) NOT NULL,
			[LastChangeTime] datetime NULL DEFAULT ''1753-01-01'',
			[OrderID] bigint NOT NULL,'
		
			if @Length='0'
			BEGIN
				SET @SQL = @SQL + '[val] ' + @DataType + ' NOT NULL,'
			END
			ELSE
			BEGIN
				SET @SQL = @SQL + '[val] ' + @DataType + '(' + @Length + ') NOT NULL,'
			
			END
		
			SET @SQL = @SQL + '[Exist]	bit NOT NULL DEFAULT 1
		)  ON [PRIMARY];
		ALTER TABLE [attT_' + @Name_Class + '_' + @Name_AttributeType + '] ADD CONSTRAINT
			[PK_' + @Name_Class + '_' + @Name_AttributeType + '] PRIMARY KEY CLUSTERED 
			(
			[GUID_Attribute_' + @Name_AttributeType + ']
			) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
		ALTER TABLE [attT_' + @Name_Class + '_' + @Name_AttributeType + '] SET (LOCK_ESCALATION = TABLE);
	END'

	EXEC(@SQL)

	SET @SQL = ''
	SET @SQL = @SQL + '
	IF OBJECT_ID(''import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + ''', N''U'') IS NULL 
	BEGIN
	
		CREATE TABLE import.[importAattT_' + @Name_Class + '_' + @Name_AttributeType + ']
		(
			[GUID_Attribute_' + @Name_AttributeType + '] varchar(36) NOT NULL,
			[GUID_AT_' + @Name_AttributeType + '] varchar(36) NOT NULL,
			[Name_AT_' + @Name_AttributeType + '] nvarchar(255) NOT NULL,
			[GUID_Object] varchar(36) NOT NULL,
			[LastChangeTime] datetime NULL DEFAULT ''1753-01-01'',
			[OrderID] bigint NOT NULL,'
		
			if @Length='0'
			BEGIN
				SET @SQL = @SQL + '[val] ' + @DataType + ' NOT NULL,'
			END
			ELSE
			BEGIN
				SET @SQL = @SQL + '[val] ' + @DataType + '(' + @Length + ') NOT NULL,'
			
			END
		
			SET @SQL = @SQL + '[Exist]	bit NOT NULL DEFAULT 1
		)  ON [PRIMARY];
		ALTER TABLE import.[importAattT_' + @Name_Class + '_' + @Name_AttributeType + '] ADD CONSTRAINT
			[PKimportAattT_' + @Name_Class + '_' + @Name_AttributeType + '] PRIMARY KEY CLUSTERED 
			(
			[GUID_Attribute_' + @Name_AttributeType + ']
			) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
		ALTER TABLE import.[importAattT_' + @Name_Class + '_' + @Name_AttributeType + '] SET (LOCK_ESCALATION = TABLE);
	END'

	EXEC(@SQL)
	
	SELECT object_id
	FROM sys.objects 
	WHERE name='attT_' + @Name_Class + '_' + @Name_AttributeType
	AND type='U'

END


GO
/****** Object:  StoredProcedure [dbo].[create_BulkTable_orgT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[create_BulkTable_orgT]
	-- Add the parameters for the stored procedure here
	 @Ontology		nvarchar(128)
	,@Name_Class	nvarchar(255)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Name_Class_Orig	nvarchar(255)
    -- Insert statements for procedure here
	DECLARE @SQL			nvarchar(max) = ''
	DECLARE @SELECT			nvarchar(max) = ''
	DECLARE @Name_Table		nvarchar(128)
	DECLARE @SET_UPdate		nvarchar(max) = ''
	DECLARE @JOIN			nvarchar(max) = ''
	DECLARE @WHERE_Insert	nvarchar(max) = ''
	
	SET @Name_Class_Orig = REPLACE(@Name_Class,'','''')
	
	
	SET @Name_Class = REPLACE(@Name_Class,'','_')
	SET @Name_Class = REPLACE(@Name_Class,' ','_')
	SET @Name_Class = REPLACE(@Name_Class,':','_')
	SET @Name_Class = REPLACE(@Name_Class,'-','_')
	SET @Name_Class = REPLACE(@Name_Class,'(','_')
	SET @Name_Class = REPLACE(@Name_Class,')','_')
	SET @Name_Class = REPLACE(@Name_Class,'/','_')
	SET @Name_Class = REPLACE(@Name_Class,'\','_')
	SET @Name_Class = REPLACE(@Name_Class,'>','_')
	SET @Name_Class = REPLACE(@Name_Class,'*','_')
	SET @Name_Class = REPLACE(@Name_Class,'.','_')
	SET @Name_Class = REPLACE(@Name_Class,',','_')	
	SET @Name_Class = REPLACE(@Name_Class,'"','_')	
	SET @Name_Class = REPLACE(@Name_Class,'&','_')	
	SET @Name_Class = REPLACE(@Name_Class,'+','_')	
	SET @Name_Class = REPLACE(@Name_Class,'%','_')	


	SET @SQL = @SQL + '
		IF OBJECT_ID(''orgT_' + @Name_Class + ''', N''U'') IS NULL 
		BEGIN
		CREATE TABLE [orgT_' + @Name_Class + ']
		(
			[GUID_' + @Name_Class +'] varchar(36) NOT NULL,
			[Name_' + @Name_Class + '] nvarchar(255) NOT NULL,
			[GUID_Class_' + @Name_Class + '] varchar(36) NOT NULL,
			[Name_Class] nvarchar(255) NOT NULL,
			[LastChangeTime] datetime NULL,
			[Exist]	bit NOT NULL DEFAULT 1
		)  ON [PRIMARY];
	ALTER TABLE [orgT_' + @Name_Class + '] ADD CONSTRAINT
		[PK_cl' + @Name_Class + '] PRIMARY KEY CLUSTERED 
		(
		[GUID_' + @Name_Class + ']
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
	ALTER TABLE [orgT_' + @Name_Class + '] SET (LOCK_ESCALATION = TABLE);
		END'
	
	exec(@SQL)

	SET @SQL = ''
	SET @SQL = @SQL + '
		IF OBJECT_ID(''import.importT_' + @Name_Class + ''', N''U'') IS NULL 
		BEGIN
			CREATE TABLE import.[importT_' + @Name_Class + ']
			(
				[GUID_' + @Name_Class +'] varchar(36) NOT NULL,
				[Name_' + @Name_Class + '] nvarchar(255) NOT NULL,
				[GUID_Class_' + @Name_Class + '] varchar(36) NOT NULL,
				[Name_Class] nvarchar(255) NOT NULL,
				[LastChangeTime] datetime NULL,
				[Exist]	bit NOT NULL DEFAULT 1
			)  ON [PRIMARY];
			ALTER TABLE import.[importT_' + @Name_Class + '] ADD CONSTRAINT
				[PK_importCl' + @Name_Class + '] PRIMARY KEY CLUSTERED 
				(
				[GUID_' + @Name_Class + ']
				) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
			ALTER TABLE import.[importT_' + @Name_Class + '] SET (LOCK_ESCALATION = TABLE);
		END'

	
	exec(@SQL)



	IF (SELECT COUNT(*)
		FROM systbl_Tables
		WHERE Name_Table='orgT_' + @Name_Class)>0 
	BEGIN
		UPDATE systbl_Tables
		SET Exist = 1, Sync_Start = GETDATE()
		WHERE Name_Table='orgT_' + @Name_Class
	END
	ELSE
	BEGIN
		INSERT INTO systbl_Tables (Type, Name_Table, Exist, Sync_Start)
		VALUES (@Ontology, 'orgT_' + @Name_Class, 1, GETDATE())
	END

	SELECT object_id
	FROM sys.objects 
	WHERE name='orgT_' + @Name_Class
	AND type='U'
END


GO
/****** Object:  StoredProcedure [dbo].[create_BulkTable_relT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[create_BulkTable_relT]
	-- Add the parameters for the stored procedure here
	 @Name_Class_Left		nvarchar(128)
	,@Name_Class_Right		nvarchar(128)
	,@Name_RelationType		nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Name_Class_Orig_Left	nvarchar(255)
    -- Insert statements for procedure here
	DECLARE @SQL			nvarchar(max) = ''
	DECLARE @SELECT			nvarchar(max) = ''
	DECLARE @Name_Table		nvarchar(128)
	DECLARE @SET_UPdate		nvarchar(max) = ''
	DECLARE @JOIN			nvarchar(max) = ''
	DECLARE @WHERE_Insert	nvarchar(max) = ''
	
	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,' ','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,':','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'-','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'(','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,')','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'/','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'\','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'>','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'*','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'.','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,',','')	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'"','')	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'&','')	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'+','')	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'%','')	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'_','')

	SET @Name_Class_Right = REPLACE(@Name_Class_Right,' ','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,':','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'-','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'(','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,')','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'/','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'\','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'>','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'*','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'.','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,',','')	
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'"','')	
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'&','')	
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'+','')	
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'%','')	
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'_','')	

	SET @Name_RelationType = REPLACE(@Name_RelationType,' ','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,':','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'-','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'(','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,')','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'/','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'\','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'>','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'*','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'.','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,',','')	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'"','')	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'&','')	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'+','')	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'%','')	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'_','')	
	
	SET @SQL = @SQL + 'IF OBJECT_ID(''[relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + ']'', N''U'') IS NULL 
	BEGIN
	
		CREATE TABLE [relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + ']
		(
			[GUID_L_' + @Name_Class_Left +'] varchar(36) NOT NULL,
			[GUID_R_' + @Name_Class_Right +'] varchar(36) NOT NULL,
			[GUID_RT_' + @Name_RelationType +'] varchar(36) NOT NULL,
			[Name_RT_' + @Name_RelationType +'] nvarchar(255) NOT NULL,
			[LastChangeTime] datetime NULL,
			[OrderID] bigint NOT NULL,
			[Exist]	bit NOT NULL DEFAULT 1
		)  ON [PRIMARY];
		ALTER TABLE [relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '] ADD CONSTRAINT
			[PK_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '] PRIMARY KEY CLUSTERED 
			(
			[GUID_L_' + @Name_Class_Left +'],[GUID_R_' + @Name_Class_Right +'],[GUID_RT_' + @Name_RelationType +']
			) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
		ALTER TABLE [relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '] SET (LOCK_ESCALATION = TABLE)
	END'

	EXEC (@SQL)

	SET @SQL = ''
	SET @SQL = @SQL + 'IF OBJECT_ID(''[import].[importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + ']'', N''U'') IS NULL 
	BEGIN
	
		CREATE TABLE [import].[importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + ']
		(
			[GUID_L_' + @Name_Class_Left +'] varchar(36) NOT NULL,
			[GUID_R_' + @Name_Class_Right +'] varchar(36) NOT NULL,
			[GUID_RT_' + @Name_RelationType +'] varchar(36) NOT NULL,
			[Name_RT_' + @Name_RelationType +'] nvarchar(255) NOT NULL,
			[LastChangeTime] datetime NULL,
			[OrderID] bigint NOT NULL,
			[Exist]	bit NOT NULL DEFAULT 1
		)  ON [PRIMARY];
		ALTER TABLE [import].[importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '] ADD CONSTRAINT
			[PKimport_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '] PRIMARY KEY CLUSTERED 
			(
			[GUID_L_' + @Name_Class_Left +'],[GUID_R_' + @Name_Class_Right +'],[GUID_RT_' + @Name_RelationType +']
			) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
		ALTER TABLE [import].[importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '] SET (LOCK_ESCALATION = TABLE)
	END'

	
	exec(@SQL)


	SELECT object_id
	FROM sys.objects 
	WHERE name= 'relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType
	AND type='U'
END



GO
/****** Object:  StoredProcedure [dbo].[create_BulkTable_relT_OR]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[create_BulkTable_relT_OR]
	-- Add the parameters for the stored procedure here
	 @Name_Class_Left		nvarchar(128)
	,@Name_RelationType		nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @SQL		nvarchar(max) = ''
	DECLARE @SELECT		nvarchar(max) = ''
	DECLARE @Name_Table	nvarchar(128)	
	DECLARE @SET_UPdate	nvarchar(max) = ''
	DECLARE @JOIN		nvarchar(max) = ''
	DECLARE @Where		nvarchar(max) = ''
	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,' ','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,':','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'-','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'(','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,')','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'/','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'\','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'>','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'*','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'.','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'_','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,',','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'"','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'&','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'+','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'%','')
	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,' ','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,':','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'-','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'(','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,')','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'/','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'\','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'>','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'*','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'.','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'_','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,',','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'"','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'&','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'+','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'%','')
	
	SET @SQL = ''
	SET @SQL = @SQL + 'IF OBJECT_ID(''[relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + ']'', N''U'') IS NULL 
	BEGIN
	
		CREATE TABLE [relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + ']
		(
			[GUID_L_' + @Name_Class_Left +'] varchar(36) NOT NULL,
			[GUID_R] varchar(36) NOT NULL,
			[GUID_RT_' + @Name_RelationType +'] varchar(36) NOT NULL,
			[Name_R] nvarchar(255) NOT NULL,
			[GUID_Parent_R] varchar(36) NULL,
			[Name_Parent_R] nvarchar(255) NULL,
			[Name_RT_' + @Name_RelationType +'] nvarchar(255) NOT NULL,
			[OrderID] bigint NOT NULL,
			[LastChangeTime] datetime NULL,
			[Exist]	bit NOT NULL DEFAULT 1
		)  ON [PRIMARY];
		ALTER TABLE [relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '] ADD CONSTRAINT
			[PK_' + @Name_Class_Left + '_To__' + @Name_RelationType + '] PRIMARY KEY CLUSTERED 
			(
			[GUID_L_' + @Name_Class_Left +'],[GUID_R],[GUID_RT_' + @Name_RelationType +']
			) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
		ALTER TABLE [relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '] SET (LOCK_ESCALATION = TABLE)
	END'

	EXEC (@SQL)
	
	SET @SQL = ''
	SET @SQL = @SQL + 'IF OBJECT_ID(''[import].[importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + ']'', N''U'') IS NULL 
	BEGIN
	
		CREATE TABLE [import].[importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + ']
		(
			[GUID_L_' + @Name_Class_Left +'] varchar(36) NOT NULL,
			[GUID_R] varchar(36) NOT NULL,
			[GUID_RT_' + @Name_RelationType +'] varchar(36) NOT NULL,
			[Name_R] nvarchar(255) NOT NULL,
			[GUID_Parent_R] varchar(36) NULL,
			[Name_Parent_R] nvarchar(255) NULL,
			[Name_RT_' + @Name_RelationType +'] nvarchar(255) NOT NULL,
			[OrderID] bigint NOT NULL,
			[LastChangeTime] datetime NULL,
			[Exist]	bit NOT NULL DEFAULT 1
		)  ON [PRIMARY];
		ALTER TABLE [import].[importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '] ADD CONSTRAINT
			[PKimport_' + @Name_Class_Left + '_To__' + @Name_RelationType + '] PRIMARY KEY CLUSTERED 
			(
			[GUID_L_' + @Name_Class_Left +'],[GUID_R],[GUID_RT_' + @Name_RelationType +']
			) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
		ALTER TABLE [import].[importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '] SET (LOCK_ESCALATION = TABLE)
	END'

	EXEC (@SQL)

SELECT object_id
FROM sys.objects 
WHERE name='relT_' + @Name_Class_Left + '_To__' + @Name_RelationType 
AND type='U'
END



GO
/****** Object:  StoredProcedure [dbo].[create_Table_attT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[create_Table_attT]
	-- Add the parameters for the stored procedure here
	 @Ontology				nvarchar(128)
	,@Name_Class			nvarchar(128)
	,@Name_AttributeType	nvarchar(128)
	,@DataType				nvarchar(128)
	,@Length				nvarchar(128)
	,@Objects				bit
	,@Path_File				nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @SQL			nvarchar(max)
	DECLARE @SQL_Table		nvarchar(max)
	DECLARE @Name_Table		nvarchar(128)
	DECLARE @SET_UPdate		nvarchar(max)
	DECLARE @SELECT			nvarchar(max)
	DECLARE @WHERE_Insert	nvarchar(max)
	DECLARE @JOIN			nvarchar(max)
	
	SET @Name_Class = REPLACE(@Name_Class,'','')
	SET @Name_Class = REPLACE(@Name_Class,' ','')
	SET @Name_Class = REPLACE(@Name_Class,':','')
	SET @Name_Class = REPLACE(@Name_Class,'-','')
	SET @Name_Class = REPLACE(@Name_Class,'(','')
	SET @Name_Class = REPLACE(@Name_Class,')','')
	SET @Name_Class = REPLACE(@Name_Class,'/','')
	SET @Name_Class = REPLACE(@Name_Class,'\','')
	SET @Name_Class = REPLACE(@Name_Class,'>','')
	SET @Name_Class = REPLACE(@Name_Class,'*','')
	SET @Name_Class = REPLACE(@Name_Class,'.','')
	SET @Name_Class = REPLACE(@Name_Class,'_','')
	SET @Name_Class = REPLACE(@Name_Class,',','')
	SET @Name_Class = REPLACE(@Name_Class,'"','')
	SET @Name_Class = REPLACE(@Name_Class,'&','')
	SET @Name_Class = REPLACE(@Name_Class,'+','')
	SET @Name_Class = REPLACE(@Name_Class,'%','')
	
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,' ','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,':','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'-','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'(','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,')','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'/','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'\','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'>','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'*','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'.','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,',','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'"','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'&','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'+','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'%','_')
	
	
	SET @SQL = '
	IF OBJECT_ID(''attT_' + @Name_Class + '_' + @Name_AttributeType + ''', N''U'') IS NULL 
	BEGIN
	
	CREATE TABLE [attT_' + @Name_Class + '_' + @Name_AttributeType + ']
	(
		[GUID_Attribute_' + @Name_AttributeType + '] varchar(36) NOT NULL,
		[GUID_AT_' + @Name_AttributeType + '] varchar(36) NOT NULL,
		[Name_AT_' + @Name_AttributeType + '] nvarchar(255) NOT NULL,
		[GUID_Object] varchar(36) NOT NULL,
		[LastChangeTime] datetime NULL DEFAULT ''1753-01-01,
		[OrderID] bigint NOT NULL,'
		
		if @Length='0'
		BEGIN
			SET @SQL = @SQL + '[val] ' + @DataType + ' NOT NULL,'
		END
		ELSE
		BEGIN
			SET @SQL = @SQL + '[val] ' + @DataType + '(' + @Length + ') NOT NULL,'
			
		END
		
		SET @SQL = @SQL + '[Exist]	bit NOT NULL DEFAULT 1
	)  ON [PRIMARY];
ALTER TABLE [attT_' + @Name_Class + '_' + @Name_AttributeType + '] ADD CONSTRAINT
	[PK_' + @Name_Class + '_' + @Name_AttributeType + '] PRIMARY KEY CLUSTERED 
	(
	[GUID_Attribute_' + @Name_AttributeType + ']
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
ALTER TABLE [attT_' + @Name_Class + '_' + @Name_AttributeType + '] SET (LOCK_ESCALATION = TABLE);
	END'

	EXEC(@SQL)
IF (SELECT COUNT(*)
	FROM systbl_Tables
	WHERE Name_Table='attT_' + @Name_Class + '_' + @Name_AttributeType)>0 
BEGIN
	UPDATE systbl_Tables
	SET Exist = 1, Sync_Start=GETDATE()
	WHERE Name_Table='attT_' + @Name_Class + '_' + @Name_AttributeType
END
ELSE
BEGIN
	INSERT INTO systbl_Tables (Type, Name_Table, Exist, Sync_Start)
	VALUES (@Ontology, 'attT_' + @Name_Class + '_' + @Name_AttributeType, 1, GETDATE())
END

SET @Name_Table = 'attT_' + @Name_Class + '_' + @Name_AttributeType

IF @Objects = 1
BEGIN
	SET @SQL_Table = 'CREATE TABLE #tmp_attt_' + @Name_Class + '_' + @Name_AttributeType + '
		(
			[GUID_Attribute_' + @Name_AttributeType +'] varchar(36) NOT NULL,
			[GUID_AT_' + @Name_AttributeType +'] varchar(36) NOT NULL,
			[Name_AT_' + @Name_AttributeType +'] nvarchar(255) NOT NULL,
			[GUID_Object] varchar(36) NOT NULL,
			[LastChangeTime] datetime NULL DEFAULT ''1753-01-01,
			[OrderID] bigint NOT NULL,'
			if @Length='0'
		BEGIN
			SET @SQL_Table = @SQL_Table + '[val] ' + @DataType + ' NOT NULL,'
		END
		ELSE
		BEGIN
			SET @SQL_Table = @SQL_Table + '[val] ' + @DataType + '(' + @Length + ') NOT NULL,'
			
		END
		SET @SQL_Table = @SQL_Table + '
		[Exist]	bit NOT NULL DEFAULT 1
		)
		
		ALTER TABLE #tmp_attt_' + @Name_Class + '_' + @Name_AttributeType + ' ADD CONSTRAINT
		[PK_tmp_' + @Name_Class + '_' + @Name_AttributeType + '] PRIMARY KEY CLUSTERED 
		(
			[GUID_Attribute]
		)'

	SET @SELECT = 'GUID_Attribute,GUID_AttributeType,GUID_Object,OrderID,val,Exist'
	SET @SET_UPdate = '[attT_' + @Name_Class + '_' + @Name_AttributeType + '].Name_AT_' + @Name_AttributeType +'=xmlset.DS.value(''Name_AttributeType[1]'',''nvarchar(max)''),[attT_' + @Name_Class + '_' + @Name_AttributeType + '].Exist=1'
	if @Length='0'
		BEGIN
			SET @SET_UPdate = @SET_UPdate + ',[val]=xmlset.DS.value(''val[1]'',''' + @DataType + ''')'
		END
		ELSE
		BEGIN
			SET @SET_UPdate = @SET_UPdate + ',[val]=xmlset.DS.value(''val[1]'',''' + @DataType + '(' + @Length + ')'')'
			
		END
	SET @WHERE_Insert = 'WHERE [attT_' + @Name_Class + '_' + @Name_AttributeType + '].GUID_Attribute_' + @Name_AttributeType +' IS NULL'
	SET @JOIN = '[attT_' + @Name_Class + '_' + @Name_AttributeType + '] ON [attT_' + @Name_Class + '_' + @Name_AttributeType + '].GUID_Attribute_' + @Name_AttributeType +'= xmlset.DS.value(''GUID_Attribute[1]'',''varchar(36)'')'
		
	execute import_BulkFile_Attribute @Ontology, @Name_Table, @SQL_Table, @SELECT, @JOIN, @WHERE_Insert, @Set_Update, @Path_File, @DataType, @Length
END

UPDATE systbl_Tables
	SET Sync_End=GETDATE()
	WHERE Name_Table=@Name_Table
	AND Type = @Ontology

SELECT object_id
FROM sys.objects 
WHERE name='attT_' + @Name_Class + '_' + @Name_AttributeType
AND type='U'

END

GO
/****** Object:  StoredProcedure [dbo].[create_Table_orgT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[create_Table_orgT]
	-- Add the parameters for the stored procedure here
	 @Ontology		nvarchar(128)
	,@Name_Class	nvarchar(255)
	,@Path_File		nvarchar(255)	
	,@Objects		bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Name_Class_Orig	nvarchar(255)
    -- Insert statements for procedure here
	DECLARE @SQL			nvarchar(max)
	DECLARE @SELECT			nvarchar(max)
	DECLARE @Name_Table		nvarchar(128)
	DECLARE @SET_UPdate		nvarchar(max)
	DECLARE @JOIN			nvarchar(max)
	DECLARE @WHERE_Insert	nvarchar(max)
	
	SET @Name_Class_Orig = REPLACE(@Name_Class,'','''')
	
	
	SET @Name_Class = REPLACE(@Name_Class,'','_')
	SET @Name_Class = REPLACE(@Name_Class,' ','_')
	SET @Name_Class = REPLACE(@Name_Class,':','_')
	SET @Name_Class = REPLACE(@Name_Class,'-','_')
	SET @Name_Class = REPLACE(@Name_Class,'(','_')
	SET @Name_Class = REPLACE(@Name_Class,')','_')
	SET @Name_Class = REPLACE(@Name_Class,'/','_')
	SET @Name_Class = REPLACE(@Name_Class,'\','_')
	SET @Name_Class = REPLACE(@Name_Class,'>','_')
	SET @Name_Class = REPLACE(@Name_Class,'*','_')
	SET @Name_Class = REPLACE(@Name_Class,'.','_')
	SET @Name_Class = REPLACE(@Name_Class,',','_')	
	SET @Name_Class = REPLACE(@Name_Class,'"','_')	
	SET @Name_Class = REPLACE(@Name_Class,'&','_')	
	SET @Name_Class = REPLACE(@Name_Class,'+','_')	
	SET @Name_Class = REPLACE(@Name_Class,'%','_')	


	SET @SQL = '
	IF OBJECT_ID(''orgT_' + @Name_Class + ''', N''U'') IS NULL 
	BEGIN
	CREATE TABLE [orgT_' + @Name_Class + ']
	(
		[GUID_' + @Name_Class +'] varchar(36) NOT NULL,
		[Name_' + @Name_Class + '] nvarchar(255) NOT NULL,
		[GUID_Class_' + @Name_Class + '] varchar(36) NOT NULL,
		[Name_Class] nvarchar(255) NOT NULL,
		[LastChangeTime] datetime NULL DEFAULT ''1753-01-01'',
		[Exist]	bit NOT NULL DEFAULT 1
	)  ON [PRIMARY];
ALTER TABLE [orgT_' + @Name_Class + '] ADD CONSTRAINT
	[PK_cl' + @Name_Class + '] PRIMARY KEY CLUSTERED 
	(
	[GUID_' + @Name_Class + ']
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
ALTER TABLE [orgT_' + @Name_Class + '] SET (LOCK_ESCALATION = TABLE);
	END'
	
exec(@SQL)



IF (SELECT COUNT(*)
	FROM systbl_Tables
	WHERE Name_Table='orgT_' + @Name_Class)>0 
BEGIN
	UPDATE systbl_Tables
	SET Exist = 1, Sync_Start = GETDATE()
	WHERE Name_Table='orgT_' + @Name_Class
END
ELSE
BEGIN
	INSERT INTO systbl_Tables (Type, Name_Table, Exist, Sync_Start)
	VALUES (@Ontology, 'orgT_' + @Name_Class, 1, GETDATE())
END

SET @Name_Table = 'orgT_' + @Name_Class

IF @Objects = 1
BEGIN
	SET @SELECT = 'GUID,Name,GUID_Class,Exist'	
	SET @SET_UPdate = '[orgT_' + @Name_Class + '].Name_' + @Name_Class + '=xmlset.DS.value(''Name[1]'',''nvarchar(255)''),[orgT_' + @Name_Class + '].LastChangeTime=xmlset.DS.value(''LastChangeTime[1]'',''datetime''),[orgT_' + @Name_Class + '].Exist=1'
	SET @WHERE_Insert = 'WHERE [orgT_' + @Name_Class + '].GUID_' + @Name_Class + ' IS NULL'
	SET @JOIN = '[orgT_' + @Name_Class + '] ON [orgT_' + @Name_Class + '].GUID_' + @Name_Class + ' = xmlset.DS.value(''GUID[1]'',''varchar(36)'')'
	
	execute import_BulkFile_Class @Ontology, @Name_Class_Orig, @Name_Table, @SELECT, @JOIN, @WHERE_Insert, @Set_Update, @Path_File
END
	
	
	UPDATE systbl_Tables
	SET Sync_End=GETDATE()
	WHERE Name_Table=@Name_Table
	AND Type = @Ontology
	
SELECT object_id
FROM sys.objects 
WHERE name='orgT_' + @Name_Class
AND type='U'

END

GO
/****** Object:  StoredProcedure [dbo].[create_Table_relT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[create_Table_relT]
	-- Add the parameters for the stored procedure here
	 @Ontology				nvarchar(128)
	,@Name_Class_Left		nvarchar(128)
	,@Name_Class_Right		nvarchar(128)
	,@Name_RelationType		nvarchar(128)
	,@Path_File				nvarchar(255)
	,@Objects				bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @SQL		nvarchar(max)
	DECLARE @SELECT		nvarchar(max)
	DECLARE @Name_Table	nvarchar(128)	
	DECLARE @SET_UPdate	nvarchar(max)
	DECLARE @JOIN		nvarchar(max)
	DECLARE @Where		nvarchar(max)
	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,' ','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,':','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'-','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'(','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,')','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'/','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'\','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'>','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'*','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'.','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'_','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,',','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'"','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'&','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'+','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'%','')
	
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,' ','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,':','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'-','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'(','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,')','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'/','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'\','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'>','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'*','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'.','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'_','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,',','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'"','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'&','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'+','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'%','')
	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,' ','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,':','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'-','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'(','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,')','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'/','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'\','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'>','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'*','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'.','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'_','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,',','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'"','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'&','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'+','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'%','')
	
	SET @SQL = 'IF OBJECT_ID(''[relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + ']'', N''U'') IS NULL 
	BEGIN
	
	CREATE TABLE [relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + ']
	(
		[GUID_L_' + @Name_Class_Left +'] varchar(36) NOT NULL,
		[GUID_R_' + @Name_Class_Right +'] varchar(36) NOT NULL,
		[GUID_RT_' + @Name_RelationType +'] varchar(36) NOT NULL,
		[Name_RT_' + @Name_RelationType +'] nvarchar(255) NOT NULL,
		[LastChangeTime] DATETIME NULL DEFAULT ''1753-01-01''
		[OrderID] bigint NOT NULL,
		[Exist]	bit NOT NULL DEFAULT 1
	)  ON [PRIMARY];
ALTER TABLE [relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '] ADD CONSTRAINT
	[PK_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '] PRIMARY KEY CLUSTERED 
	(
	[GUID_L_' + @Name_Class_Left +'],[GUID_R_' + @Name_Class_Right +'],[GUID_RT_' + @Name_RelationType +']
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
ALTER TABLE [relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '] SET (LOCK_ESCALATION = TABLE)
END'

	EXEC (@SQL)
	
	IF (SELECT COUNT(*)
	FROM systbl_Tables
	WHERE Name_Table='relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType)>0 
BEGIN
	UPDATE systbl_Tables
	SET Exist = 1, Sync_Start = GETDATE()
	WHERE Name_Table='relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType
END
ELSE
BEGIN
	INSERT INTO systbl_Tables (Type, Name_Table, Exist, Sync_Start)
	VALUES (@Ontology, 'relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType, 1, GETDATE())
END
	
	SET @SELECT = 'GUID_Object_Left,GUID_Object_Right,GUID_RelationType, Name_RelationType, OrderID, Exist'
	SET @SET_UPdate = 'relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType 
		+'.Name_RT_' + @Name_RelationType +'=xmlset.DS.value(''Name_RelationType[1]'',''nvarchar(255)''),'
		+'relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.OrderID=xmlset.DS.value(''OrderID[1]'',''bigint''),'
		+'relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.Exist=1'
	SET @JOIN = 'relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + ' ON relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType 
	+'.GUID_L_' + @Name_Class_Left +'=xmlset.DS.value(''GUID_Object_Left[1]'',''varchar(36)'') AND '
	+'relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType 
	+'.GUID_R_' + @Name_Class_Right +'=xmlset.DS.value(''GUID_Object_Right[1]'',''varchar(36)'') AND '
	+'relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType 
	+'.GUID_RT_' + @Name_RelationType +'=xmlset.DS.value(''GUID_RelationType[1]'',''varchar(36)'')'
	SET @Name_Table = 'relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType	
	SET @Where = @Name_Table + '.GUID_L_' + @Name_Class_Left + ' IS NULL
		AND ' + @Name_Table + '.GUID_R_' + @Name_Class_Right + ' IS NULL
		AND ' + @Name_Table + '.GUID_RT_' + @Name_RelationType + ' IS NULL'
	if @Objects=1
	BEGIN

		execute import_BulkFile_RelationType @Ontology, @Name_Table, @SELECT, @Join, @Where, @SET_UPdate, @Path_File
	END




UPDATE systbl_Tables
SET Sync_End=GETDATE()
WHERE Name_Table=@Name_Table
AND Type = @Ontology

SELECT object_id
FROM sys.objects 
WHERE name='relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType 
AND type='U'
END

GO
/****** Object:  StoredProcedure [dbo].[create_Table_relT_OR]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[create_Table_relT_OR]
	-- Add the parameters for the stored procedure here
	 @Ontology				nvarchar(128)
	,@Name_Class_Left		nvarchar(128)
	,@Name_RelationType		nvarchar(128)
	,@Path_File				nvarchar(255)
	,@Objects				bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @SQL		nvarchar(max)
	DECLARE @SELECT		nvarchar(max)
	DECLARE @Name_Table	nvarchar(128)	
	DECLARE @SET_UPdate	nvarchar(max)
	DECLARE @JOIN		nvarchar(max)
	DECLARE @Where		nvarchar(max)
	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,' ','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,':','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'-','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'(','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,')','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'/','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'\','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'>','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'*','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'.','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'_','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,',','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'"','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'&','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'+','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'%','')
	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,' ','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,':','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'-','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'(','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,')','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'/','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'\','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'>','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'*','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'.','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'_','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,',','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'"','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'&','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'+','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'%','')
	
	SET @SQL = 'IF OBJECT_ID(''[relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + ']'', N''U'') IS NULL 
	BEGIN
	
	CREATE TABLE [relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + ']
	(
		[GUID_L_' + @Name_Class_Left +'] varchar(36) NOT NULL,
		[GUID_R] varchar(36) NOT NULL,
		[Name_R] nvarchar(255) NOT NULL,
		[GUID_Parent_R] varchar(36) NULL,
		[Name_Parent_R] nvarchar(255) NULL,
		[GUID_RT_' + @Name_RelationType +'] varchar(36) NOT NULL,
		[Name_RT_' + @Name_RelationType +'] nvarchar(255) NOT NULL,
		[OrderID] bigint NOT NULL,
		[LastChangeTime] datetime NULL DEFAULT ''1753-01-01'',
		[Exist]	bit NOT NULL DEFAULT 1
	)  ON [PRIMARY];
ALTER TABLE [relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '] ADD CONSTRAINT
	[PK_' + @Name_Class_Left + '_To__' + @Name_RelationType + '] PRIMARY KEY CLUSTERED 
	(
	[GUID_L_' + @Name_Class_Left +'],[GUID_R],[GUID_RT_' + @Name_RelationType +']
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
ALTER TABLE [relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '] SET (LOCK_ESCALATION = TABLE)
END'

	EXEC (@SQL)
	
	IF (SELECT COUNT(*)
	FROM systbl_Tables
	WHERE Name_Table='relT_' + @Name_Class_Left + '_To__' + @Name_RelationType)>0 
BEGIN
	UPDATE systbl_Tables
	SET Exist = 1, Sync_Start = GETDATE()
	WHERE Name_Table='relT_' + @Name_Class_Left + '_To__' + @Name_RelationType
END
ELSE
BEGIN
	INSERT INTO systbl_Tables (Type, Name_Table, Exist, Sync_Start)
	VALUES (@Ontology, 'relT_' + @Name_Class_Left + '_To__' + @Name_RelationType, 1, GETDATE())
END
	
	SET @SELECT = 'GUID_Object_Left, GUID_Object_Right, GUID_Parent_Right, Name_Prent_Right, GUID_RelationType, Name_RelationType, OrderID, Exist'
	SET @SET_UPdate = 'relT_' + @Name_Class_Left + '_To__' + @Name_RelationType 
		+'.Name_RT_' + @Name_RelationType +'=xmlset.DS.value(''Name_RelationType[1]'',''nvarchar(255)''),'
		+'relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.OrderID=xmlset.DS.value(''OrderID[1]'',''bigint''),'
		+'relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.Name_R=xmlset.DS.value(''Name_Right[1]'',''nvarchar(255)''),'
		+'relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_Parent_R=xmlset.DS.value(''GUID_Parent_Right[1]'',''varchar(36)''),'
		+'relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.Name_Parent_R=xmlset.DS.value(''Name_Parent_Right[1]'',''nvarchar(255)''),'
		+'relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.LastChangeTime=xmlset.DS.value(''LastChangeTime[1]'',''datetime''),'
		+'relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.Exist=1'
	SET @JOIN = 'relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + ' ON relT_' + @Name_Class_Left + '_To__' + @Name_RelationType 
	+'.GUID_L_' + @Name_Class_Left +'=xmlset.DS.value(''GUID_Object_Left[1]'',''varchar(36)'') AND '
	+'relT_' + @Name_Class_Left + '_To__' + @Name_RelationType 
	+'.GUID_R=xmlset.DS.value(''GUID_Right[1]'',''varchar(36)'') AND '
	+'relT_' + @Name_Class_Left + '_To__' + @Name_RelationType 
	+'.GUID_RT_' + @Name_RelationType +'=xmlset.DS.value(''GUID_RelationType[1]'',''varchar(36)'')'
	SET @Name_Table = 'relT_' + @Name_Class_Left + '_To__' + @Name_RelationType	
	SET @Where = @Name_Table + '.GUID_L_' + @Name_Class_Left + ' IS NULL
		AND ' + @Name_Table + '.GUID_R IS NULL
		AND ' + @Name_Table + '.GUID_RT_' + @Name_RelationType + ' IS NULL'
	if @Objects=1
	BEGIN

		execute import_BulkFile_RelationType_OR @Ontology, @Name_Table, @SELECT, @Join, @Where, @SET_UPdate, @Path_File
	END




UPDATE systbl_Tables
SET Sync_End=GETDATE()
WHERE Name_Table=@Name_Table
AND Type = @Ontology

SELECT object_id
FROM sys.objects 
WHERE name='relT_' + @Name_Class_Left + '_To__' + @Name_RelationType 
AND type='U'
END


GO
/****** Object:  StoredProcedure [dbo].[delete_ImportTables]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[delete_ImportTables] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @SQL		NVARCHAR(MAX) = ''

	DECLARE cur_importTables CURSOR FOR
		SELECT 'DELETE FROM [import].' + obj.name
		FROM sys.all_objects obj
		INNER JOIN sys.schemas s ON obj.schema_id = s.schema_id
		WHERE [type] = 'U' AND s.name = 'import'
	
	OPEN cur_importTables
	FETCH NEXT FROM cur_importTables INTO
		@SQL
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC(@SQL)
		FETCH NEXT FROM cur_importTables INTO
			@SQL
	END

	CLOSE cur_importTables
	DEALLOCATE cur_importTables
	
END
GO
/****** Object:  StoredProcedure [dbo].[finalize_Table]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[finalize_Table]
	-- Add the parameters for the stored procedure here
	@Name_Table		nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @SQL	nvarchar(max)
    
    SET @Name_Table = REPLACE(@Name_Table,'','_')
	SET @Name_Table = REPLACE(@Name_Table,' ','_')
	SET @Name_Table = REPLACE(@Name_Table,':','_')
	SET @Name_Table = REPLACE(@Name_Table,'-','_')
	SET @Name_Table = REPLACE(@Name_Table,'(','_')
	SET @Name_Table = REPLACE(@Name_Table,')','_')
	SET @Name_Table = REPLACE(@Name_Table,'/','_')
	SET @Name_Table = REPLACE(@Name_Table,'\','_')
	SET @Name_Table = REPLACE(@Name_Table,'>','_')
	SET @Name_Table = REPLACE(@Name_Table,'*','_')
	SET @Name_Table = REPLACE(@Name_Table,'.','_')
	SET @Name_Table = REPLACE(@Name_Table,',','_')
	SET @Name_Table = REPLACE(@Name_Table,'"','_')
	SET @Name_Table = REPLACE(@Name_Table,'&','_')
	SET @Name_Table = REPLACE(@Name_Table,'+','_')
	SET @Name_Table = REPLACE(@Name_Table,'%','_')
    
	SET @SQL = 'DELETE FROM ' + @Name_Table + '
		WHERE Exist=0'
	EXEC(@SQL)
	
	SELECT 1 AS Done
END

GO
/****** Object:  StoredProcedure [dbo].[finalize_Table_attT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[finalize_Table_attT]
	-- Add the parameters for the stored procedure here
	 @Name_Class			nvarchar(128)
	,@Name_AttributeType	nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @SQL	nvarchar(max)
    
    SET @Name_Class = REPLACE(@Name_Class,'','')
	SET @Name_Class = REPLACE(@Name_Class,' ','')
	SET @Name_Class = REPLACE(@Name_Class,':','')
	SET @Name_Class = REPLACE(@Name_Class,'-','')
	SET @Name_Class = REPLACE(@Name_Class,'(','')
	SET @Name_Class = REPLACE(@Name_Class,')','')
	SET @Name_Class = REPLACE(@Name_Class,'/','')
	SET @Name_Class = REPLACE(@Name_Class,'\','')
	SET @Name_Class = REPLACE(@Name_Class,'>','')
	SET @Name_Class = REPLACE(@Name_Class,'*','')
	SET @Name_Class = REPLACE(@Name_Class,'.','')
	SET @Name_Class = REPLACE(@Name_Class,'_','')
	SET @Name_Class = REPLACE(@Name_Class,',','')
	SET @Name_Class = REPLACE(@Name_Class,'"','')
	SET @Name_Class = REPLACE(@Name_Class,'&','')
	SET @Name_Class = REPLACE(@Name_Class,'+','')
	SET @Name_Class = REPLACE(@Name_Class,'%','')
	
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,' ','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,':','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'-','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'(','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,')','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'/','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'\','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'>','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'*','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'.','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,',','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'"','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'&','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'+','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'%','_')
    
	SET @SQL = 'DELETE FROM [attT_' + @Name_Class + '_' + @Name_AttributeType + ']
		WHERE Exist=0'
	EXEC(@SQL)
	
	SELECT 1 AS Done
END

GO
/****** Object:  StoredProcedure [dbo].[finalize_Table_orgT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[finalize_Table_orgT]
	-- Add the parameters for the stored procedure here
	 @Name_Class	nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @SQL	nvarchar(max)
    
    SET @Name_Class = REPLACE(@Name_Class,'','_')
	SET @Name_Class = REPLACE(@Name_Class,' ','_')
	SET @Name_Class = REPLACE(@Name_Class,':','_')
	SET @Name_Class = REPLACE(@Name_Class,'-','_')
	SET @Name_Class = REPLACE(@Name_Class,'(','_')
	SET @Name_Class = REPLACE(@Name_Class,')','_')
	SET @Name_Class = REPLACE(@Name_Class,'/','_')
	SET @Name_Class = REPLACE(@Name_Class,'\','_')
	SET @Name_Class = REPLACE(@Name_Class,'>','_')
	SET @Name_Class = REPLACE(@Name_Class,'*','_')
	SET @Name_Class = REPLACE(@Name_Class,'.','_')
	SET @Name_Class = REPLACE(@Name_Class,',','_')	
	SET @Name_Class = REPLACE(@Name_Class,'"','_')	
	SET @Name_Class = REPLACE(@Name_Class,'&','_')	
	SET @Name_Class = REPLACE(@Name_Class,'+','_')	
	SET @Name_Class = REPLACE(@Name_Class,'%','_')	
    
	SET @SQL = 'DELETE FROM [orgT_' + @Name_Class + ']
		WHERE Exist=0'
	EXEC(@SQL)
	
	SELECT 1 AS Done
END

GO
/****** Object:  StoredProcedure [dbo].[finalize_Table_relT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[finalize_Table_relT]
	-- Add the parameters for the stored procedure here
	 @Name_Class_Left		nvarchar(128)
	,@Name_Class_Right		nvarchar(128)
	,@Name_RelationType		nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @SQL	nvarchar(max)
    
    SET @Name_Class_Left = REPLACE(@Name_Class_Left,'','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,' ','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,':','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'-','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'(','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,')','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'/','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'\','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'>','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'*','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'.','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'_','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,',','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'"','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'&','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'+','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'%','')
	
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,' ','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,':','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'-','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'(','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,')','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'/','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'\','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'>','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'*','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'.','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'_','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,',','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'"','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'&','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'+','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'%','')
	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,' ','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,':','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'-','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'(','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,')','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'/','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'\','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'>','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'*','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'.','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'_','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,',','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'"','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'&','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'+','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'%','')	
    
	SET @SQL = 'DELETE FROM [relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + ']
		WHERE Exist=0'
	EXEC(@SQL)
	
	SELECT 1 AS Done
END

GO
/****** Object:  StoredProcedure [dbo].[finalize_Table_relT_OR]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[finalize_Table_relT_OR]
	-- Add the parameters for the stored procedure here
	 @Name_Class_Left		nvarchar(128)
	,@Name_RelationType		nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @SQL	nvarchar(max)
    
    SET @Name_Class_Left = REPLACE(@Name_Class_Left,'','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,' ','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,':','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'-','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'(','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,')','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'/','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'\','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'>','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'*','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'.','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'_','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,',','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'"','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'&','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'+','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'%','')
	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,' ','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,':','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'-','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'(','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,')','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'/','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'\','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'>','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'*','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'.','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'_','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,',','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'"','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'&','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'+','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'%','')
    
	SET @SQL = 'DELETE FROM [relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + ']
		WHERE Exist=0'
	EXEC(@SQL)
	
	SELECT 1 AS Done
END

GO
/****** Object:  StoredProcedure [dbo].[finalize_Tables]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[finalize_Tables] 
	-- Add the parameters for the stored procedure here
	@Type			nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @SQL		nvarchar(max)
    DECLARE @Name_Table	nvarchar(128)
    
    DECLARE cur_Tables CURSOR FOR
		SELECT Name_Table
		FROM systbl_Tables
		WHERE [Type] = @Type
		AND Exist = 0
		
	OPEN cur_Tables
	FETCH NEXT FROM cur_Tables INTO
		@Name_Table
		
	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @SQL = 'DROP TABLE [' + @Name_Table + ']'
		
		exec(@SQL)
		
		FETCH NEXT FROM cur_Tables INTO
			@Name_Table
	END
	CLOSE cur_Tables
	DEALLOCATE cur_Tables
	
	DELETE FROM systbl_Tables
	WHERE Exist = 0
	AND [Type] = @Type
	
	SELECT	 Type
			,Name_Table
			,Exist
	FROM systbl_Tables
	WHERE Exist = 0
	AND [Type] = @Type
END

GO
/****** Object:  StoredProcedure [dbo].[import_BulkFile]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[import_BulkFile] 
	-- Add the parameters for the stored procedure here
	 @Name_Table		nvarchar(128)
	,@SQL_TMPTABLE		nvarchar(max)
	,@SELECT			nvarchar(max)
	,@Join				nvarchar(max)
	,@Where_Insert		nvarchar(max)
	,@Set_Update		nvarchar(max)
	,@Path_File			nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @SQL		nvarchar(max)
	
	EXEC(@SQL)
	
	SET @SQL = @SQL_TMPTABLE + '
		INSERT INTO #tmp_' + @Name_Table + '
		SELECT ' +  @SELECT + '
		FROM OPENROWSET(
		BULK ''' + @Path_File + ''',
		SINGLE_BLOB) AS tmp_Table
		
		INSERT INTO [' + @Name_Table + ']
		SELECT * FROM #tmp_' + @Name_Table + '
		LEFT OUTER JOIN ' + @Join + ' '
		+ @WHERE_Insert
		+ '
		
		UPDATE [' + @Name_Table + '] 
		SET ' + @Set_Update + '
		FROM #tmp_' + @Name_Table + '
		INNER JOIN ' + @Join + ' '
	print @SQL
	exec(@SQL)
	
	
END

GO
/****** Object:  StoredProcedure [dbo].[import_BulkFile_Attribute]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[import_BulkFile_Attribute] 
	-- Add the parameters for the stored procedure here
	 @Ontology			nvarchar(255)
	,@Name_Table		nvarchar(128)
	,@SQL_TMPTABLE		nvarchar(max)
	,@SELECT			nvarchar(max)
	,@Join				nvarchar(max)
	,@Where_Insert		nvarchar(max)
	,@Set_Update		nvarchar(max)
	,@Path_File			nvarchar(255)
	,@DataType			nvarchar(128)
	,@Length			nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @SQL		nvarchar(max)

	
	SET @SQL = '
	
		CREATE TABLE #tmp_xml
		(
			XMLCol xml
		)
		
		INSERT INTO #tmp_xml
		SELECT *
		FROM OPENROWSET(
		BULK ''' + @Path_File + ''',
		SINGLE_BLOB) AS tmp_Table
		
		INSERT INTO [' + @Name_Table + ']
		SELECT	 xmlset.DS.value(''GUID_Attribute[1]'',''varchar(36)'') AS GUID_Attribute
			,xmlset.DS.value(''GUID_AttributeType[1]'',''varchar(36)'') AS GUID_AttributeType
			,xmlset.DS.value(''Name_AttributeType[1]'',''nvarchar(max)'') AS Name_AttributeType
			,xmlset.DS.value(''GUID_Object[1]'',''varchar(36)'') AS GUID_Object
			,xmlset.DS.value(''LastChangeTime'',''datetime'') AS LastChangeTime
			,xmlset.DS.value(''OrderID[1]'',''bigint'') AS OrderID'
		if Not @Length='0'
		BEGIN
		SET @SQL = @SQL + ',xmlset.DS.value(''val[1]'',''' + @DataType + '(' + @Length +')'') AS Val'
		END
		ELSE
		BEGIN
			SET @SQL = @SQL + ',xmlset.DS.value(''val[1]'',''' + @DataType +''') AS Val'
		END
			
		SET @SQL = @SQL + '
			,1 AS Exist
		FROM #tmp_xml tmp_table
		CROSS APPLY XMLCol.nodes(''/root/tmptbl'') AS xmlset(DS)
		LEFT OUTER JOIN ' + @Join + ' '
		+ @WHERE_Insert
		+ '
		
		UPDATE [' + @Name_Table + '] 
		SET ' + @Set_Update + '
		FROM #tmp_xml tmp_table
		CROSS APPLY XMLCol.nodes(''/root/tmptbl'') AS xmlset(DS)
		INNER JOIN ' + @Join + ' '
		PRINT @SQL
	exec(@SQL)

END

GO
/****** Object:  StoredProcedure [dbo].[import_BulkFile_Class]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[import_BulkFile_Class] 
	-- Add the parameters for the stored procedure here
	 @Ontology			nvarchar(255)
	,@Name_Class		nvarchar(255)
	,@Name_Table		nvarchar(128)
	,@SELECT			nvarchar(max)
	,@Join				nvarchar(max)
	,@Where_Insert		nvarchar(max)
	,@Set_Update		nvarchar(max)
	,@Path_File			nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @SQL		nvarchar(max)

	
	SET @SQL = '
	
		CREATE TABLE #tmp_xml
		(
			XMLCol xml
		)
		
		INSERT INTO #tmp_xml
		SELECT *
		FROM OPENROWSET(
		BULK ''' + @Path_File + ''',
		SINGLE_BLOB) AS tmp_Table
		
		INSERT INTO [' + @Name_Table + ']
		SELECT	 xmlset.DS.value(''GUID[1]'',''varchar(36)'') AS GUID
			,xmlset.DS.value(''Name[1]'',''nvarchar(max)'') AS Name
			,xmlset.DS.value(''GUID_Class[1]'',''varchar(36)'') AS GUID_Class
			,''' + @Name_Class + ''' AS Name_Class
			,xmlset.DS.value(''LastChangeTime[1]'',''varchar(36)'') AS LastChangeTime
			,xmlset.DS.value(''Exist[1]'',''bit'') AS Exist
		FROM #tmp_xml tmp_table
		CROSS APPLY XMLCol.nodes(''/root/tmptbl'') AS xmlset(DS)
		LEFT OUTER JOIN ' + @Join + ' '
		+ @WHERE_Insert
		+ '
		
		UPDATE [' + @Name_Table + '] 
		SET ' + @Set_Update + '
		FROM #tmp_xml tmp_table
		CROSS APPLY XMLCol.nodes(''/root/tmptbl'') AS xmlset(DS)
		INNER JOIN ' + @Join + ' '
	PRINT @SQL
	exec(@SQL)

END

GO
/****** Object:  StoredProcedure [dbo].[import_BulkFile_RelationType]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[import_BulkFile_RelationType] 
	-- Add the parameters for the stored procedure here
	 @Ontology			nvarchar(255)
	,@Name_Table		nvarchar(128)
	,@SELECT			nvarchar(max)
	,@Join				nvarchar(max)
	,@Where				nvarchar(max)
	,@Set_Update		nvarchar(max)
	,@Path_File			nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @SQL		nvarchar(max)

	
	SET @SQL = '
	
		CREATE TABLE #tmp_xml
		(
			XMLCol xml
		)
		
		INSERT INTO #tmp_xml
		SELECT *
		FROM OPENROWSET(
		BULK ''' + @Path_File + ''',
		SINGLE_BLOB) AS tmp_Table
		
		INSERT INTO [' + @Name_Table + ']
		SELECT	 xmlset.DS.value(''GUID_Object_Left[1]'',''varchar(36)'') AS GUID_Object_Left
			,xmlset.DS.value(''GUID_Object_Right[1]'',''varchar(36)'') AS GUID_Object_Right
			,xmlset.DS.value(''GUID_RelationType[1]'',''varchar(36)'') AS GUID_RelationType
			,xmlset.DS.value(''Name_RelationType[1]'',''nvarchar(max)'') AS Name_RelationType
			,xmlset.DS.value(''OrderID[1]'',''bigint'') AS OrderID
			,xmlset.DS.value(''LastChangeTime[1]'',''datetime'') AS LastChangeTime
			,xmlset.DS.value(''Exist[1]'',''bit'') AS Exist
		FROM #tmp_xml tmp_table
		CROSS APPLY XMLCol.nodes(''/root/tmptbl'') AS xmlset(DS)
		LEFT OUTER JOIN ' + @Join + ' 
		WHERE ' + @Where + '
		
		UPDATE [' + @Name_Table + '] 
		SET ' + @Set_Update + '
		FROM #tmp_xml tmp_table
		CROSS APPLY XMLCol.nodes(''/root/tmptbl'') AS xmlset(DS)
		INNER JOIN ' + @Join  + ' '

		print @SQL
	exec(@SQL)

END

GO
/****** Object:  StoredProcedure [dbo].[import_BulkFile_RelationType_OR]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[import_BulkFile_RelationType_OR] 
	-- Add the parameters for the stored procedure here
	 @Ontology			nvarchar(255)
	,@Name_Table		nvarchar(128)
	,@SELECT			nvarchar(max)
	,@Join				nvarchar(max)
	,@Where				nvarchar(max)
	,@Set_Update		nvarchar(max)
	,@Path_File			nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @SQL		nvarchar(max)

	
	SET @SQL = '
	
		CREATE TABLE #tmp_xml
		(
			XMLCol xml
		)
		
		INSERT INTO #tmp_xml
		SELECT *
		FROM OPENROWSET(
		BULK ''' + @Path_File + ''',
		SINGLE_BLOB) AS tmp_Table
		
		INSERT INTO [' + @Name_Table + ']
		SELECT	 xmlset.DS.value(''GUID_Object_Left[1]'',''varchar(36)'') AS GUID_Object_Left
			,xmlset.DS.value(''GUID_Right[1]'',''varchar(36)'') AS GUID_Right
			,xmlset.DS.value(''Name_Right[1]'',''nvarchar(255)'') AS Name_Right
			,xmlset.DS.value(''GUID_Parent_Right[1]'',''varchar(36)'') AS GUID_Parent_Right
			,xmlset.DS.value(''Name_Parent_Right[1]'',''nvarchar(255)'') AS Name_Parent_Right
			,xmlset.DS.value(''GUID_RelationType[1]'',''varchar(36)'') AS GUID_RelationType
			,xmlset.DS.value(''Name_RelationType[1]'',''nvarchar(255)'') AS Name_RelationType
			,xmlset.DS.value(''OrderID[1]'',''bigint'') AS OrderID
			,xmlset.DS.value(''Exist[1]'',''bit'') AS Exist
		FROM #tmp_xml tmp_table
		CROSS APPLY XMLCol.nodes(''/root/tmptbl'') AS xmlset(DS)
		LEFT OUTER JOIN ' + @Join + ' 
		WHERE ' + @Where + '
		
		UPDATE [' + @Name_Table + '] 
		SET ' + @Set_Update + '
		FROM #tmp_xml tmp_table
		CROSS APPLY XMLCol.nodes(''/root/tmptbl'') AS xmlset(DS)
		INNER JOIN ' + @Join  + ' '

	exec(@SQL)

END


GO
/****** Object:  StoredProcedure [dbo].[initialize_Table]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[initialize_Table]
	-- Add the parameters for the stored procedure here
	@Name_Table		nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET @Name_Table = REPLACE(@Name_Table,'','_')
	SET @Name_Table = REPLACE(@Name_Table,' ','_')
	SET @Name_Table = REPLACE(@Name_Table,':','_')
	SET @Name_Table = REPLACE(@Name_Table,'-','_')
	SET @Name_Table = REPLACE(@Name_Table,'(','_')
	SET @Name_Table = REPLACE(@Name_Table,')','_')
	SET @Name_Table = REPLACE(@Name_Table,'/','_')
	SET @Name_Table = REPLACE(@Name_Table,'\','_')
	SET @Name_Table = REPLACE(@Name_Table,'>','_')
	SET @Name_Table = REPLACE(@Name_Table,'*','_')
	SET @Name_Table = REPLACE(@Name_Table,'.','_')
	SET @Name_Table = REPLACE(@Name_Table,',','_')
	SET @Name_Table = REPLACE(@Name_Table,'"','_')
	SET @Name_Table = REPLACE(@Name_Table,'&','_')
	SET @Name_Table = REPLACE(@Name_Table,'+','_')
	SET @Name_Table = REPLACE(@Name_Table,'%','_')

    -- Insert statements for procedure here
    DECLARE @SQL	nvarchar(max)
    
	SET @SQL = '
		IF OBJECT_ID(''' + @Name_Table + ''', N''U'') IS NOT NULL 
		BEGIN
			UPDATE ' + @Name_Table + '
			SET Exist=0
		END'
	
	EXEC(@SQL)
	
	SELECT 1 AS Done
END

GO
/****** Object:  StoredProcedure [dbo].[initialize_Table_AttT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[initialize_Table_AttT]
	-- Add the parameters for the stored procedure here
	 @Name_Class			nvarchar(128)
	,@Name_AttributeType	nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET @Name_Class = REPLACE(@Name_Class,'','')
	SET @Name_Class = REPLACE(@Name_Class,' ','')
	SET @Name_Class = REPLACE(@Name_Class,':','')
	SET @Name_Class = REPLACE(@Name_Class,'-','')
	SET @Name_Class = REPLACE(@Name_Class,'(','')
	SET @Name_Class = REPLACE(@Name_Class,')','')
	SET @Name_Class = REPLACE(@Name_Class,'/','')
	SET @Name_Class = REPLACE(@Name_Class,'\','')
	SET @Name_Class = REPLACE(@Name_Class,'>','')
	SET @Name_Class = REPLACE(@Name_Class,'*','')
	SET @Name_Class = REPLACE(@Name_Class,'.','')
	SET @Name_Class = REPLACE(@Name_Class,'_','')
	SET @Name_Class = REPLACE(@Name_Class,',','')
	SET @Name_Class = REPLACE(@Name_Class,'"','')
	SET @Name_Class = REPLACE(@Name_Class,'&','')
	SET @Name_Class = REPLACE(@Name_Class,'+','')
	SET @Name_Class = REPLACE(@Name_Class,'%','')
	
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,' ','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,':','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'-','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'(','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,')','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'/','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'\','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'>','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'*','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'.','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,',','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'"','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'&','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'+','_')
	SET @Name_AttributeType = REPLACE(@Name_AttributeType,'%','_')
	
    -- Insert statements for procedure here
    DECLARE @SQL	nvarchar(max)
    
	SET @SQL = '
		IF OBJECT_ID(''attT_' + @Name_Class + '_' + @Name_AttributeType + ''') IS NOT NULL 
		BEGIN
			UPDATE [attT_' + @Name_Class + '_' + @Name_AttributeType + ']
			SET Exist=0
		END'
	
	EXEC(@SQL)
	
	SELECT 1 AS Done
END


GO
/****** Object:  StoredProcedure [dbo].[initialize_Table_orgT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[initialize_Table_orgT]
	-- Add the parameters for the stored procedure here
	 @Name_Class			nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET @Name_Class = REPLACE(@Name_Class,'','_')
	SET @Name_Class = REPLACE(@Name_Class,' ','_')
	SET @Name_Class = REPLACE(@Name_Class,':','_')
	SET @Name_Class = REPLACE(@Name_Class,'-','_')
	SET @Name_Class = REPLACE(@Name_Class,'(','_')
	SET @Name_Class = REPLACE(@Name_Class,')','_')
	SET @Name_Class = REPLACE(@Name_Class,'/','_')
	SET @Name_Class = REPLACE(@Name_Class,'\','_')
	SET @Name_Class = REPLACE(@Name_Class,'>','_')
	SET @Name_Class = REPLACE(@Name_Class,'*','_')
	SET @Name_Class = REPLACE(@Name_Class,'.','_')
	SET @Name_Class = REPLACE(@Name_Class,',','_')	
	SET @Name_Class = REPLACE(@Name_Class,'"','_')	
	SET @Name_Class = REPLACE(@Name_Class,'&','_')	
	SET @Name_Class = REPLACE(@Name_Class,'+','_')	
	SET @Name_Class = REPLACE(@Name_Class,'%','_')	
	
    -- Insert statements for procedure here
    DECLARE @SQL	nvarchar(max)
    
	SET @SQL = '
		IF OBJECT_ID(''orgT_' + @Name_Class + ''') IS NOT NULL 
		BEGIN
			UPDATE [orgT_' + @Name_Class + ']
			SET Exist=0
		END'
	print @SQL
	EXEC(@SQL)
	
	SELECT 1 AS Done
END



GO
/****** Object:  StoredProcedure [dbo].[initialize_Table_relT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[initialize_Table_relT]
	-- Add the parameters for the stored procedure here
	 @Name_Class_Left		nvarchar(128)
	,@Name_Class_Right		nvarchar(128)
	,@Name_RelationType		nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,' ','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,':','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'-','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'(','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,')','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'/','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'\','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'>','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'*','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'.','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'_','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,',','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'"','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'&','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'+','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'%','')
	
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,' ','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,':','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'-','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'(','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,')','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'/','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'\','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'>','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'*','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'.','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'_','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,',','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'"','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'&','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'+','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'%','')
	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,' ','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,':','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'-','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'(','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,')','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'/','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'\','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'>','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'*','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'.','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'_','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,',','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'"','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'&','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'+','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'%','')
	
    -- Insert statements for procedure here
    DECLARE @SQL	nvarchar(max)
    
	SET @SQL = '
		IF OBJECT_ID(''[relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + ']'') IS NOT NULL 
		BEGIN
			UPDATE [relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + ']
			SET Exist=0
		END'
	
	EXEC(@SQL)
	
	SELECT 1 AS Done
END



GO
/****** Object:  StoredProcedure [dbo].[initialize_Table_relT_OR]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[initialize_Table_relT_OR]
	-- Add the parameters for the stored procedure here
	 @Name_Class_Left		nvarchar(128)
	,@Name_RelationType		nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,' ','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,':','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'-','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'(','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,')','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'/','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'\','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'>','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'*','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'.','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'_','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,',','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'"','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'&','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'+','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'%','')
	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,' ','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,':','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'-','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'(','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,')','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'/','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'\','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'>','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'*','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'.','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'_','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,',','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'"','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'&','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'+','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'%','')
	
    -- Insert statements for procedure here
    DECLARE @SQL	nvarchar(max)
    
	SET @SQL = '
		IF OBJECT_ID(''[relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + ']'') IS NOT NULL 
		BEGIN
			UPDATE [relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + ']
			SET Exist=0
		END'
	
	EXEC(@SQL)
	
	SELECT 1 AS Done
END



GO
/****** Object:  StoredProcedure [dbo].[initialize_Tables]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[initialize_Tables] 
	-- Add the parameters for the stored procedure here
	@Type		nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE systbl_Tables
	SET Exist=0
	WHERE [Type] = @Type
	
	SELECT *
	FROM systbl_Tables
	WHERE Exist=1
	AND [Type] = @Type
END

GO
/****** Object:  StoredProcedure [dbo].[insert_Object]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insert_Object] 
	-- Add the parameters for the stored procedure here
	 @GUID_Session		varchar(36)
	,@GUID_Class		varchar(36)
	,@GUID_Object		varchar(36)
	,@Name_Object		nvarchar(255)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @SQL		nvarchar(max)
	
	SET @SQL = 'INSERT INTO [@TABLE@] VALUES (''@GUID_OBJECT@'',''@NAME_OBJECT@'',''@GUID_CLASS@'')'
	
	SET @SQL = REPLACE(@SQL,'@TABLE@',@GUID_Session+@GUID_Class)
	SET @SQL = REPLACE(@SQL,'@GUID_OBJECT@',@GUID_Object)
	SET @SQL = REPLACE(@SQL,'@NAME_OBJECT@',@Name_Object)
	SET @SQL = REPLACE(@SQL,'@GUID_CLASS@',@GUID_Class)
	PRINT @SQL
	EXEC(@SQL)
END

GO
/****** Object:  StoredProcedure [dbo].[orgproc_Fill_DateTable]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[orgproc_Fill_DateTable]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @Start	DATETIME
	DECLARE @End	DATETIME

    IF OBJECT_ID('dbo.Datetable') IS NULL
	BEGIN
		CREATE TABLE dbo.Datetable
		(
			TimeStampId int NOT NULL IDENTITY (1, 1),
			TimeStamp datetime NOT NULL,
			Year int NOT NULL,
			Month int NOT NULL,
			Day int NOT NULL,
			Weekday nvarchar(128) NOT NULL,
			Week int NOT NULL,
			Dateseq int NOT NULL,
			Quarter int NOT NULL,
			TwoMonth int NOT NULL
		)  ON [PRIMARY]
	
		ALTER TABLE dbo.Datetable ADD CONSTRAINT
			PK_Datetable PRIMARY KEY CLUSTERED 
			(
			TimeStampId
			) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	
		CREATE UNIQUE NONCLUSTERED INDEX IX_Datetable ON dbo.Datetable
			(
			TimeStamp
			) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
		ALTER TABLE dbo.Datetable SET (LOCK_ESCALATION = TABLE)
	END

	SELECT @Start = MAX([TimeStamp])
	FROM dbo.Datetable

	SET @End = DATEADD(year,5,@Start)

	IF @Start IS NULL
	BEGIN
		SET @Start = '1972-01-01'
	END
	SET @Start = DATEADD(Day, 1, @Start)
	WHILE @Start < @End
	BEGIN
		INSERT INTO Datetable VALUES (
			@Start,
			DATEPART(Year, @Start),
			DATEPART(Month, @Start),
			DATEPART(Day, @Start),
			DATENAME(WEEKDAY, @Start),
			DATENAME(WW, @Start),
			DATEPART(Year, @Start) * 10000 +
			DATEPART(Month, @Start) * 100 +
			DATEPART(Day, @Start),
			DATEPART(QQ, @Start),
			CASE WHEN DATEPART(Month, @Start) < 3 THEN 1
			ELSE CASE WHEN DATEPART(Month, @Start) < 5 THEN 2
			ELSE CASE WHEN DATEPART(Month, @Start) < 7 THEN 3
			ELSE CASE WHEN DATEPART(Month, @Start) < 9 THEN 4
			ELSE CASE WHEN DATEPART(Month, @Start) < 11 THEN 5
			ELSE CASE WHEN DATEPART(Month, @Start) < 13 THEN 6
			END END END END END END
		)
		SET @Start = DATEADD(Day, 1, @Start)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sysproc_Code]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sysproc_Code]
	-- Add the parameters for the stored procedure here
	@RoutineName		nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @tmp_Code TABLE (
		Code	NVARCHAR(255)
	)
	
	INSERT INTO @tmp_Code 
	EXEC sp_helptext @RoutineName

	SELECT Code
	FROM @tmp_Code
END

GO
/****** Object:  StoredProcedure [dbo].[sysproc_reorganize_indexes]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sysproc_reorganize_indexes]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @name_table	nvarchar(128)
	DECLARE @name_schema nvarchar(128)
	DECLARE @code nvarchar(max)
	DECLARE cur_name CURSOR FOR
		SELECT t.name AS [table_name], s.name AS [schema_name]
		FROM sys.tables t
		INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
		WHERE type='U'
		AND NOT s.name LIKE '%import%'

	OPEN cur_name

	FETCH NEXT FROM cur_name INTO
		 @name_table
		,@name_schema

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @code = 'ALTER INDEX ALL ON ont_db_reports.' + @name_schema + '.' + @name_table	+ ' REORGANIZE;'

		Print @code
		BEGIN TRY
			EXECUTE sp_executesql @code
		END TRY
		BEGIN CATCH

		END CATCH
		FETCH NEXT FROM cur_name INTO
			 @name_table
			,@name_schema
	END
	CLOSE cur_name
	DEALLOCATE cur_name

END

GO
/****** Object:  StoredProcedure [dbo].[update_Table_attT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_Table_attT]
	-- Add the parameters for the stored procedure here
	  @Name_Class			nvarchar(255)
	 ,@Name_AttributeType	nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @SQL	nvarchar(max) = ''
    
    SET @Name_Class = REPLACE(@Name_Class,'','_')
	SET @Name_Class = REPLACE(@Name_Class,' ','_')
	SET @Name_Class = REPLACE(@Name_Class,':','_')
	SET @Name_Class = REPLACE(@Name_Class,'-','_')
	SET @Name_Class = REPLACE(@Name_Class,'(','_')
	SET @Name_Class = REPLACE(@Name_Class,')','_')
	SET @Name_Class = REPLACE(@Name_Class,'/','_')
	SET @Name_Class = REPLACE(@Name_Class,'\','_')
	SET @Name_Class = REPLACE(@Name_Class,'>','_')
	SET @Name_Class = REPLACE(@Name_Class,'*','_')
	SET @Name_Class = REPLACE(@Name_Class,'.','_')
	SET @Name_Class = REPLACE(@Name_Class,',','_')	
	SET @Name_Class = REPLACE(@Name_Class,'"','_')	
	SET @Name_Class = REPLACE(@Name_Class,'&','_')	
	SET @Name_Class = REPLACE(@Name_Class,'+','_')	
	SET @Name_Class = REPLACE(@Name_Class,'%','_')	
    

	SET @SQL = @SQL + 'INSERT INTO attt_' + @Name_Class + '_' + @Name_AttributeType + '
		SELECT import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[GUID_Attribute_' + @Name_AttributeType + ']
			  ,import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[GUID_AT_' + @Name_AttributeType + ']
			  ,import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[Name_AT_' + @Name_AttributeType + ']
			  ,import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[GUID_Object]
			  ,import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[LastChangeTime]
			  ,import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[OrderId]
			  ,import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[Val]
			  ,import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[Exist]
		FROM import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '
		LEFT OUTER JOIN attt_' + @Name_Class + '_' + @Name_AttributeType + ' ON import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.GUID_Attribute_' + @Name_AttributeType + ' = attt_' + @Name_Class + '_' + @Name_AttributeType + '.GUID_Attribute_' + @Name_AttributeType + '
		WHERE attt_' + @Name_Class + '_' + @Name_AttributeType + '.GUID_Attribute_' + @Name_AttributeType + ' IS NULL

		UPDATE attt_' + @Name_Class + '_' + @Name_AttributeType + '
		SET  attt_' + @Name_Class + '_' + @Name_AttributeType + '.[GUID_AT_' + @Name_AttributeType + '] = import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[GUID_AT_' + @Name_AttributeType + ']
			,attt_' + @Name_Class + '_' + @Name_AttributeType + '.[Name_AT_' + @Name_AttributeType + '] = import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[Name_AT_' + @Name_AttributeType + ']
			,attt_' + @Name_Class + '_' + @Name_AttributeType + '.[GUID_Object] = import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[GUID_Object]
			,attt_' + @Name_Class + '_' + @Name_AttributeType + '.[LastChangeTime] = import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[LastChangeTime]
			,attt_' + @Name_Class + '_' + @Name_AttributeType + '.[OrderId] = import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[OrderId]
			,attt_' + @Name_Class + '_' + @Name_AttributeType + '.[val] = import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[val]
			,attt_' + @Name_Class + '_' + @Name_AttributeType + '.[Exist] = import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[Exist]
		FROM import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '
		INNER JOIN attt_' + @Name_Class + '_' + @Name_AttributeType + ' ON import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.GUID_Attribute_' + @Name_AttributeType + ' = attt_' + @Name_Class + '_' + @Name_AttributeType + '.GUID_Attribute_' + @Name_AttributeType + '

		DELETE attt_' + @Name_Class + '_' + @Name_AttributeType + '
		FROM attt_' + @Name_Class + '_' + @Name_AttributeType + '
		LEFT OUTER JOIN import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + ' ON attt_' + @Name_Class + '_' + @Name_AttributeType + '.[GUID_Attribute_' + @Name_AttributeType + '] = import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[GUID_Attribute_' + @Name_AttributeType + ']
		WHERE import.importAattT_' + @Name_Class + '_' + @Name_AttributeType + '.[GUID_Attribute_' + @Name_AttributeType + '] IS NULL'
	EXEC(@SQL)

	SET @SQL = 'DELETE FROM import.importAattT_' + @Name_Class + '_' + @Name_AttributeType
	EXEC(@SQL)
	
	SELECT 1 AS Done
END


GO
/****** Object:  StoredProcedure [dbo].[update_Table_orgT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_Table_orgT]
	-- Add the parameters for the stored procedure here
	 @Name_Class	nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @SQL	nvarchar(max) = ''
    
    SET @Name_Class = REPLACE(@Name_Class,'','_')
	SET @Name_Class = REPLACE(@Name_Class,' ','_')
	SET @Name_Class = REPLACE(@Name_Class,':','_')
	SET @Name_Class = REPLACE(@Name_Class,'-','_')
	SET @Name_Class = REPLACE(@Name_Class,'(','_')
	SET @Name_Class = REPLACE(@Name_Class,')','_')
	SET @Name_Class = REPLACE(@Name_Class,'/','_')
	SET @Name_Class = REPLACE(@Name_Class,'\','_')
	SET @Name_Class = REPLACE(@Name_Class,'>','_')
	SET @Name_Class = REPLACE(@Name_Class,'*','_')
	SET @Name_Class = REPLACE(@Name_Class,'.','_')
	SET @Name_Class = REPLACE(@Name_Class,',','_')	
	SET @Name_Class = REPLACE(@Name_Class,'"','_')	
	SET @Name_Class = REPLACE(@Name_Class,'&','_')	
	SET @Name_Class = REPLACE(@Name_Class,'+','_')	
	SET @Name_Class = REPLACE(@Name_Class,'%','_')	
    
	SET @SQL = @SQL + 'INSERT INTO orgt_' + @Name_Class + '
		SELECT import.importT_' + @Name_Class + '.[GUID_' + @Name_Class + ']
			  ,import.importT_' + @Name_Class + '.[Name_' + @Name_Class + ']
			  ,import.importT_' + @Name_Class + '.[GUID_Class_' + @Name_Class + ']
			  ,import.importT_' + @Name_Class + '.[Name_Class]
			  ,import.importT_' + @Name_Class + '.[LastChangeTime]
			  ,import.importT_' + @Name_Class + '.[Exist]
		FROM import.importT_' + @Name_Class + '
		LEFT OUTER JOIN orgt_' + @Name_Class + ' ON importT_' + @Name_Class + '.GUID_' + @Name_Class + ' = orgt_' + @Name_Class + '.GUID_' + @Name_Class + '
		WHERE orgt_' + @Name_Class + '.GUID_' + @Name_Class + ' IS NULL

		UPDATE orgt_' + @Name_Class + '
		SET  orgt_' + @Name_Class + '.[Name_' + @Name_Class + '] = import.importT_' + @Name_Class + '.[Name_' + @Name_Class + ']
			,orgt_' + @Name_Class + '.[GUID_Class_' + @Name_Class + '] = import.importT_' + @Name_Class + '.[GUID_Class_' + @Name_Class + ']
			,orgt_' + @Name_Class + '.[Name_Class] = import.importT_' + @Name_Class + '.[Name_Class]
			,orgt_' + @Name_Class + '.[LastChangeTime] = import.importT_' + @Name_Class + '.[LastChangeTime]
		FROM import.importT_' + @Name_Class + '
		INNER JOIN orgt_' + @Name_Class + ' ON import.importT_' + @Name_Class + '.GUID_' + @Name_Class + ' = orgt_' + @Name_Class + '.GUID_' + @Name_Class + '

		DELETE orgt_' + @Name_Class + '
		FROM orgt_' + @Name_Class + '
		LEFT OUTER JOIN import.importT_' + @Name_Class + ' ON orgt_' + @Name_Class + '.[GUID_' + @Name_Class + '] = import.importT_' + @Name_Class + '.[GUID_' + @Name_Class + ']
		WHERE import.importT_' + @Name_Class + '.[GUID_' + @Name_Class + '] IS NULL'
	EXEC(@SQL)
	
	SET @SQL = 'DELETE FROM import.importT_' + @Name_Class
	EXEC(@SQL)

	SELECT 1 AS Done
END


GO
/****** Object:  StoredProcedure [dbo].[update_Table_relT]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_Table_relT]
	-- Add the parameters for the stored procedure here
	 @Name_Class_Left		nvarchar(128)
	,@Name_Class_Right		nvarchar(128)
	,@Name_RelationType		nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @SQL	nvarchar(max) = ''
    
    SET @Name_Class_Left = REPLACE(@Name_Class_Left,' ','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,':','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'-','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'(','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,')','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'/','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'\','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'>','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'*','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'.','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,',','')	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'"','')	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'&','')	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'+','')	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'%','')	

	SET @Name_Class_Right = REPLACE(@Name_Class_Right,' ','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,':','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'-','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'(','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,')','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'/','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'\','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'>','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'*','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'.','')
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,',','')	
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'"','')	
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'&','')	
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'+','')	
	SET @Name_Class_Right = REPLACE(@Name_Class_Right,'%','')	

	SET @Name_RelationType = REPLACE(@Name_RelationType,' ','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,':','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'-','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'(','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,')','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'/','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'\','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'>','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'*','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'.','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,',','')	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'"','')	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'&','')	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'+','')	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'%','')	
    
	SET @SQL = @SQL + 'INSERT INTO relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '
		SELECT import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[GUID_L_' + @Name_Class_Left +']
			  ,import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[GUID_R_' + @Name_Class_Right +']
			  ,import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[GUID_RT_' + @Name_RelationType +']
			  ,import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[Name_RT_' + @Name_RelationType +']
			  ,import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[LastChangeTime]
			  ,import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[OrderID]
			  ,import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[Exist]
		FROM import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '
		LEFT OUTER JOIN relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + ' ON 
				import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + ' = relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + '
			AND	import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_R_' + @Name_Class_Right + ' = relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_R_' + @Name_Class_Right + '
			AND	import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_RT_' + @Name_RelationType + ' = relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_RT_' + @Name_RelationType + '
		WHERE relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + ' IS NULL

		UPDATE relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '
		SET  relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[Name_RT_' + @Name_RelationType +'] = import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[Name_RT_' + @Name_RelationType +']
			,relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[OrderID] = import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[OrderID]
			,relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[LastChangeTime] = import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[LastChangeTime]
			,relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[Exist] = import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.[Exist]
		FROM import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '
		INNER JOIN relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + ' ON 
				import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + ' = relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + '
			AND	import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_R_' + @Name_Class_Right + ' = relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_R_' + @Name_Class_Right + '
			AND	import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_RT_' + @Name_RelationType + ' = relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_RT_' + @Name_RelationType + '

		DELETE relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '
		FROM relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '
		LEFT OUTER JOIN import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + ' ON 
				import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + ' = relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + '
			AND	import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_R_' + @Name_Class_Right + ' = relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_R_' + @Name_Class_Right + '
			AND	import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_RT_' + @Name_RelationType + ' = relT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_RT_' + @Name_RelationType + '
		WHERE import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + ' IS NULL'
	EXEC(@SQL)

	SET @SQL = 'DELETE FROM import.importRelT_' + @Name_Class_Left + '_To_' + @Name_Class_Right + '_' + @Name_RelationType
	EXEC(@SQL)
	
	SELECT 1 AS Done
END



GO
/****** Object:  StoredProcedure [dbo].[update_Table_relT_OR]    Script Date: 17.03.2024 09:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_Table_relT_OR]
	-- Add the parameters for the stored procedure here
	 @Name_Class_Left		nvarchar(128)
	,@Name_RelationType		nvarchar(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @SQL	nvarchar(max) = ''
    
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,' ','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,':','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'-','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'(','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,')','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'/','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'\','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'>','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'*','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'.','')
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,',','')	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'"','')	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'&','')	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'+','')	
	SET @Name_Class_Left = REPLACE(@Name_Class_Left,'%','')	

	SET @Name_RelationType = REPLACE(@Name_RelationType,' ','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,':','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'-','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'(','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,')','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'/','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'\','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'>','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'*','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,'.','')
	SET @Name_RelationType = REPLACE(@Name_RelationType,',','')	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'"','')	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'&','')	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'+','')	
	SET @Name_RelationType = REPLACE(@Name_RelationType,'%','')	
    
	SET @SQL = @SQL + 'INSERT INTO relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + ' ([GUID_L_' + @Name_Class_Left +']
		, [GUID_R]
		, [GUID_RT_' + @Name_RelationType +']
		, [Name_R]
		, [GUID_Parent_R]
		, [Name_Parent_R]
		, [Name_RT_' + @Name_RelationType +']
		, [OrderID]
		, [LastChangeTime]
		, [Exist])
		SELECT import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[GUID_L_' + @Name_Class_Left +']
			  ,import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[GUID_R]
			  ,import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[GUID_RT_' + @Name_RelationType +']
			  ,import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[Name_R]
			  ,import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[GUID_Parent_R]
			  ,import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[Name_Parent_R]
			  ,import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[Name_RT_' + @Name_RelationType +']
			  ,import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[OrderID]
			  ,import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[LastChangeTime]
			  ,import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[Exist]
		FROM import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '
		LEFT OUTER JOIN relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + ' ON 
				import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + ' = relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + '
			AND	import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_RT_' + @Name_RelationType + ' = relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_RT_' + @Name_RelationType + '
			AND	import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_R = relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_R
		WHERE relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + ' IS NULL

		UPDATE relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '
		SET  relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[Name_R] = import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[Name_R]
			,relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[GUID_Parent_R] = import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[GUID_Parent_R]
			,relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[Name_Parent_R] = import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[Name_Parent_R]
			,relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[Name_RT_' + @Name_RelationType +'] = import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[Name_RT_' + @Name_RelationType +']
			,relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[OrderID] = import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[OrderID]
			,relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[LastChangeTime] = import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[LastChangeTime]
			,relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[Exist] = import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.[Exist]
		FROM import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '
		INNER JOIN relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + ' ON 
				import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + ' = relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + '
			AND	import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_RT_' + @Name_RelationType + ' = relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_RT_' + @Name_RelationType + '
			AND	import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_R = relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_R

		DELETE relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '
		FROM relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '
		LEFT OUTER JOIN import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + ' ON 
				import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + ' = relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + '
			AND	import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_RT_' + @Name_RelationType + ' = relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_RT_' + @Name_RelationType + '
			AND	import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_R = relT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_R
		WHERE import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType + '.GUID_L_' + @Name_Class_Left + ' IS NULL'
	EXEC(@SQL)
	
	SET @SQL = 'DELETE FROM import.importRelT_' + @Name_Class_Left + '_To__' + @Name_RelationType
	EXEC(@SQL)

	SELECT 1 AS Done
END



GO
